#include "TabelaSimbola.h"
#include "Ucitavanje.h"

vector<TabelaSimbola> TabelaSimbola::nizSimbola = init();

unsigned int  TabelaSimbola::dohvatiLokacijuSimbola(string s){
	for(unsigned int i=0;i<nizSimbola.size();i++){
		if((nizSimbola[i].labela.compare(s)==0)&&(nizSimbola[i].sekcija.compare("UNDEFINED")!=0)){
			unsigned int rez= nizSimbola[i].ofset;
			rez+=Ucitavanje::dohvatiLokacijuSekcije(nizSimbola[i].sekcija, nizSimbola[i].fajl);
			return rez;
		}
	}
	cout<<"simbol nigde nije definisan "<<s<<endl;
	throw 1;
}

unsigned int TabelaSimbola::dohvatiLokacijuTrazenogSimbola(unsigned int redniBr, string fajl){
	for(unsigned int i=0;i<nizSimbola.size();i++){
		if((nizSimbola[i].redniBr==redniBr) && (nizSimbola[i].fajl.compare(fajl)==0)){
			if(nizSimbola[i].sekcija.compare("UNDEFINED")==0){
				for(unsigned int j=0;j<nizSimbola.size();j++){
					if(i==j)
						continue;
					if(nizSimbola[i].labela.compare(nizSimbola[j].labela)==0){
						unsigned int rez= nizSimbola[j].ofset;
						rez+=Ucitavanje::dohvatiLokacijuSekcije(nizSimbola[j].sekcija, nizSimbola[j].fajl);
						return rez;
					}
				}
				cout<<"simbol nigde nije definisan "<<redniBr<<endl;
				throw 1;
			}else{
				unsigned int rez= nizSimbola[i].ofset;
				rez+=Ucitavanje::dohvatiLokacijuSekcije(nizSimbola[i].sekcija, nizSimbola[i].fajl);
				return rez;
			}
		}
	}
	cout<<"simbol nigde nije definisan "<<redniBr<<endl;
	throw 1;
}

string TabelaSimbola::dohvatiSekciju(unsigned int redniBr, string fajl){
	for(unsigned int i=0;i<nizSimbola.size();i++){
		if((nizSimbola[i].redniBr==redniBr) && (nizSimbola[i].fajl.compare(fajl)==0)){
			return nizSimbola[i].sekcija;
		}
	}
	cout<<"simbol nije definisan nigde "<<redniBr<<endl;
	throw 1;
}

TabelaSimbola::TabelaSimbola(string l, string s, string lo, 
	int o, unsigned int r, string f) {
    labela = l;
    sekcija = s;
    lokal = lo;
    ofset = o;
    redniBr = r;
    fajl=f;
    nizSimbola.push_back(TabelaSimbola(this));
}

TabelaSimbola::TabelaSimbola(TabelaSimbola *t) {
    labela = t->labela;
    sekcija = t->sekcija;
    lokal = t->lokal;
    ofset = t->ofset;
    redniBr = t->redniBr;
    fajl=t->fajl;
}

bool TabelaSimbola::pronadji(string s) {
    for (unsigned int i = 0; i < nizSimbola.size(); i++)
        if (s.compare(nizSimbola[i].labela) == 0)
            return true;
    return false;
}

vector<TabelaSimbola> TabelaSimbola::init() {
    TabelaSimbola tab = TabelaSimbola("*UNDEFINED*:", "-2", "L", 0, 0,"UNDF");
    vector<TabelaSimbola> t;
    t.push_back(tab);
    return t;
}
