#ifndef __TabelaSimbola_H_
#define __TabelaSimbola_H_

#include <iostream>
#include <cstring>
#include <vector>
using namespace std;

class TabelaSimbola {
	public:
		TabelaSimbola(string l, string s, string lo, 
			 int o, unsigned int r, string f);
		static bool pronadji(string s);
		string labela, sekcija, lokal, fajl;
		unsigned int ofset, redniBr, lokacija;
		static vector<TabelaSimbola> nizSimbola;
		static vector<TabelaSimbola> init();
		TabelaSimbola(TabelaSimbola *t);
		static unsigned int dohvatiLokacijuTrazenogSimbola(unsigned int redniBr, string fajl);
		static string dohvatiSekciju(unsigned int redniBr, string fajl);
		static unsigned int dohvatiLokacijuSimbola(string s);
};

#endif

