#ifndef __Interpreter_H_
#define __Interpreter_H_

#include <iostream>
#include <cstring>
#include <vector>
using namespace std;

typedef struct{
	string ime;
	string lokacija;
} ulazniParametri;

class Interpreter {
	private:
		static string pswZ, pswO, pswC, pswI, pswN, pswTr, pswTl;
		static string toPSW();
		static int r0, r1, r2, r3, r4, r5, sp, pc;
		static short rTEMP;
		static void dodajNaStek2(string s);
		static void dodajNaStek1(string s);
		static string skiniSaStek2();
		static string skiniSaStek1();
		static int dohvatiVrednostRegistra(string s);
		static void postaviVrednostRegistra(string s, int i);
		static void postaviPSW(string s);
		static string dohvatiBitUHex(string s, int i);
		static void postaviPSWN(int i);
		static void postaviPSWZ(int i);
		static void sigalrm_handler(int sig);
	public:
		static vector<ulazniParametri> nizUlaznihParametara;
		static string memorija[];
		static void pokreni(vector<char *> ulazFajl);
		static void izvrsiInstrukciju();
		static void upisiUMemoriju1(string s, unsigned int lokacija);
		static void upisiUMemoriju2(string s, unsigned int lokacija);
		static bool proveraSllobodnostiMemorije(unsigned int i, unsigned int j);
		static unsigned int vratiSlobodnuLokaciju(unsigned int velicina);
		static unsigned int mainLokacija;
		static unsigned int ivTableLokacija;
};

#endif


