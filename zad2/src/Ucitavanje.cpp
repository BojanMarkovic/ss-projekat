#include "Ucitavanje.h"
#include "Interpreter.h"
#include "Konvert.h"
#include "TabelaSimbola.h"

vector<jedanFajl> Ucitavanje::fajlovi;
unsigned int Ucitavanje::defaultLokacija=256;
string Ucitavanje::trenutnaLokacija;
string Ucitavanje::trenutnaLokacijaSekcija;
unsigned int Ucitavanje::lokacijaTabeleSimbola=0;

void Ucitavanje::napuniLokalMemoriju(){
	for(unsigned int o=0;o<fajlovi.size();o++){
		vector<string> tokeni= fajlovi[o].tokeni;
		for(unsigned int i=0; i<tokeni.size();i++){
			if(tokeni[i].compare("TabelaSekcija")==0){
				trenutnaLokacija="TabelaSekcija";
				continue;
			}
			if(tokeni[i].compare("TabelaSimbola")==0){
				trenutnaLokacija="TabelaSimbola";
				continue;
			}
			if(tokeni[i].compare("TabelaRelokacije")==0){
				trenutnaLokacija="TabelaRelokacije";
				continue;
			}
			if(i+1<tokeni.size()){
				if(tokeni[i+1].compare("PXR")==0){
					trenutnaLokacijaSekcija=tokeni[i];
					sekcijeT x;
					x.ime=tokeni[i];
					x.flegovi="PXR";
					x.pocetnaLokacija=0;
					x.krajnjaLokacija=0;
					x.odradjen=false;
					fajlovi[o].tabelaSekcija.push_back(x);
					i++;
					continue;
				}
				if(tokeni[i+1].compare("RW")==0){
					trenutnaLokacijaSekcija=tokeni[i];
					sekcijeT x;
					x.ime=tokeni[i];
					x.flegovi="RW";
					x.pocetnaLokacija=0;
					x.krajnjaLokacija=0;
					x.odradjen=false;
					fajlovi[o].tabelaSekcija.push_back(x);
					i++;
					continue;
				}
				if(tokeni[i+1].compare("PRW")==0){
					trenutnaLokacijaSekcija=tokeni[i];
					sekcijeT x;
					x.ime=tokeni[i];
					x.flegovi="PRW";
					x.pocetnaLokacija=0;
					x.krajnjaLokacija=0;
					x.odradjen=false;
					fajlovi[o].tabelaSekcija.push_back(x);
					i++;
					continue;
				}
			}
			if(trenutnaLokacija.compare("TabelaSekcija")==0){
				for(unsigned int j=0;j<fajlovi[o].tabelaSekcija.size();j++){
					if(trenutnaLokacijaSekcija.compare(fajlovi[o].tabelaSekcija[j].ime)==0){
						if(fajlovi[o].tabelaSekcija[j].flegovi.compare("PXR")==0){
							fajlovi[o].tabelaSekcija[j].mem.push_back(tokeni[i]);
						}
						if(fajlovi[o].tabelaSekcija[j].flegovi.compare("PRW")==0){
							fajlovi[o].tabelaSekcija[j].mem.push_back(tokeni[i]);
						}
						if(fajlovi[o].tabelaSekcija[j].flegovi.compare("RW")==0){
							unsigned int x;   
							std::stringstream ss2;
							ss2 << tokeni[i];
							ss2 >> x;
							for(unsigned int p=0;p<x;p++){
								fajlovi[o].tabelaSekcija[j].mem.push_back("00");
							}

						}
						break;
					}
					continue;
				}	
				continue;
			}
			if(trenutnaLokacija.compare("TabelaSimbola")==0){
				string labela = tokeni[i++];
				string sekcija = tokeni[i++];
				string lokal = tokeni[i++];   
				int ofset;
				std::stringstream ss;
				ss << tokeni[i++];
				ss >> ofset;
				unsigned int redniBr;
				std::stringstream ss1;
				ss1 << tokeni[i];
				ss1 >> redniBr;
				TabelaSimbola(labela, sekcija, lokal, ofset, redniBr, fajlovi[o].ime);
				continue;
			}
		}
		trenutnaLokacija="UNDF";
	}
}

void Ucitavanje::postaviPocetneLokacije(){
	for(unsigned int i=0;i<fajlovi.size();i++){
		for(unsigned int j=0;j<fajlovi[i].tabelaSekcija.size();j++){
			for(unsigned int o=0;o<Interpreter::nizUlaznihParametara.size();o++){
				if(fajlovi[i].tabelaSekcija[j].ime.compare(Interpreter::nizUlaznihParametara[o].ime)==0){
					fajlovi[i].tabelaSekcija[j].pocetnaLokacija=Konvert::konvertujStringToUnInt(
						Interpreter::nizUlaznihParametara[o].lokacija);
					fajlovi[i].tabelaSekcija[j].krajnjaLokacija=fajlovi[i].tabelaSekcija[j].pocetnaLokacija;
					Interpreter::nizUlaznihParametara.erase(Interpreter::nizUlaznihParametara.begin()+o,Interpreter::nizUlaznihParametara.begin()+o+1);
					break;
				}
				fajlovi[i].tabelaSekcija[j].pocetnaLokacija=0;
			}
		}
	}
}

void Ucitavanje::napuniMemoriju(){
	for(unsigned int i=0;i<fajlovi.size();i++){
		for(unsigned int j=0;j<fajlovi[i].tabelaSekcija.size();j++){
			if(fajlovi[i].tabelaSekcija[j].pocetnaLokacija!=0){
				fajlovi[i].tabelaSekcija[j].odradjen=true;
				if(Interpreter::proveraSllobodnostiMemorije(fajlovi[i].tabelaSekcija[j].pocetnaLokacija, fajlovi[i].tabelaSekcija[j].mem.size())){
					cout<<"ne moze se smestiti na zeljenu lokaciju, vec je zauzeta"<<endl;
					throw 1;
				}
				for(unsigned int p=0;p<fajlovi[i].tabelaSekcija[j].mem.size();p++){
					Interpreter::memorija[fajlovi[i].tabelaSekcija[j].krajnjaLokacija]=fajlovi[i].tabelaSekcija[j].mem[p];
					fajlovi[i].tabelaSekcija[j].krajnjaLokacija++;
				}
			}
		}
	}
	for(unsigned int i=0;i<fajlovi.size();i++){
		for(unsigned int j=0;j<fajlovi[i].tabelaSekcija.size();j++){
			if(fajlovi[i].tabelaSekcija[j].odradjen==false){
				fajlovi[i].tabelaSekcija[j].odradjen=true;
				fajlovi[i].tabelaSekcija[j].pocetnaLokacija=nadjiNajvisu();
				fajlovi[i].tabelaSekcija[j].krajnjaLokacija=fajlovi[i].tabelaSekcija[j].pocetnaLokacija;
				if(Interpreter::proveraSllobodnostiMemorije(fajlovi[i].tabelaSekcija[j].pocetnaLokacija, fajlovi[i].tabelaSekcija[j].mem.size())){
					fajlovi[i].tabelaSekcija[j].pocetnaLokacija=Interpreter::vratiSlobodnuLokaciju(fajlovi[i].tabelaSekcija[j].mem.size());
				}
				fajlovi[i].tabelaSekcija[j].krajnjaLokacija=fajlovi[i].tabelaSekcija[j].pocetnaLokacija;
				for(unsigned int p=0;p<fajlovi[i].tabelaSekcija[j].mem.size();p++){
					Interpreter::memorija[fajlovi[i].tabelaSekcija[j].krajnjaLokacija]=fajlovi[i].tabelaSekcija[j].mem[p];
					fajlovi[i].tabelaSekcija[j].krajnjaLokacija++;
				}		
			}
		}
	}
}

unsigned int Ucitavanje::nadjiNajvisu(){
	unsigned int max=0;
	for(unsigned int i=0;i<fajlovi.size();i++){
		for(unsigned int j=0;j<fajlovi[i].tabelaSekcija.size();j++){
			if(fajlovi[i].tabelaSekcija[j].odradjen==true){
				if(max<fajlovi[i].tabelaSekcija[j].krajnjaLokacija)
					max=fajlovi[i].tabelaSekcija[j].krajnjaLokacija;
			}
		}
	}
	if(max==0)
		return 16;
	return max;
}

void Ucitavanje::razresiSimbole(){
	for(unsigned int i=0;i<TabelaSimbola::nizSimbola.size()-1;i++){
		for(unsigned int j=i+1;j<TabelaSimbola::nizSimbola.size();j++){
			if((TabelaSimbola::nizSimbola[i].labela.compare(TabelaSimbola::nizSimbola[j].labela)==0)
				&&(TabelaSimbola::nizSimbola[i].lokal.compare("G")==0)
				&&(TabelaSimbola::nizSimbola[j].lokal.compare("G")==0)){
				if((TabelaSimbola::nizSimbola[i].sekcija.compare("UNDEFINED")==0)||
					(TabelaSimbola::nizSimbola[j].sekcija.compare("UNDEFINED")==0)){
					continue;
				}else{
					cout<<"dupli simboli: "<<TabelaSimbola::nizSimbola[i].labela<<endl;
					throw 1;
				}	
			}
		}
	}
	for(unsigned int o=0;o<fajlovi.size();o++){
		vector<string> tokeni= fajlovi[o].tokeni;
		for(unsigned int i=0; i<tokeni.size();i++){
			if(tokeni[i].compare("TabelaSekcija")==0){
				trenutnaLokacija="TabelaSekcija";
				continue;
			}
			if(tokeni[i].compare("TabelaSimbola")==0){
				trenutnaLokacija="TabelaSimbola";
				continue;
			}
			if(tokeni[i].compare("TabelaRelokacije")==0){
				trenutnaLokacija="TabelaRelokacije";
				continue;
			}

			if(trenutnaLokacija.compare("TabelaRelokacije")==0){
				string sekcija=tokeni[i++];
				string tip=tokeni[i++];
				string ofset=tokeni[i++];
				unsigned int redniBr;  
				stringstream ss;
				ss << tokeni[i];
				ss >> redniBr;
				int lokacija = TabelaSimbola::dohvatiLokacijuTrazenogSimbola(redniBr, fajlovi[o].ime);
				unsigned int mojaLokacija=dohvatiLokacijuSekcije(sekcija, fajlovi[o].ime)+
					Konvert::konvertujStringToUnInt(ofset);
				if(tip.compare("R_386_16")==0){
					int temp=Konvert::konvertujStringToInt(Interpreter::memorija[mojaLokacija]
						+Interpreter::memorija[mojaLokacija+1]);
					temp+=lokacija;
					string s=Konvert::intToHexUn(temp);
					Interpreter::memorija[mojaLokacija]=s.substr(0,2);
					mojaLokacija++;
					Interpreter::memorija[mojaLokacija]=s.substr(2,2);
				}else if(tip.compare("R_386_PC16")==0){
					int temp=Konvert::konvertujStringToInt(Interpreter::memorija[mojaLokacija]
						+Interpreter::memorija[mojaLokacija+1]);
					unsigned int q =dohvatiLokacijuSekcije(sekcija, fajlovi[o].ime);
					temp+=lokacija-q;
					temp-=2;
					string s=Konvert::vratiHexNa4B(Konvert::intToHex(temp));
					Interpreter::memorija[mojaLokacija]=s.substr(0,2);
					mojaLokacija++;
					Interpreter::memorija[mojaLokacija]=s.substr(2,2);
				}
				continue;
			}
		}
	}
	trenutnaLokacija="UNDF";
}

unsigned int Ucitavanje::dohvatiLokacijuSekcije(string sekcija, string fajl){
	for(unsigned int i=0;i<fajlovi.size();i++){
		if(fajlovi[i].ime.compare(fajl)==0){
			for(unsigned int j=0;j<fajlovi[i].tabelaSekcija.size();j++){
				if(fajlovi[i].tabelaSekcija[j].ime.compare(sekcija)==0){
					return fajlovi[i].tabelaSekcija[j].pocetnaLokacija;
				}
			}
		}
	}
	cout<<"simbol nije pronadjen "<<endl;
	throw 1;
	return 0;
}

void Ucitavanje::oslobodiVektore(){
	for(unsigned int i=0;i<fajlovi.size();i++){
		fajlovi[i].tokeni.clear();
		for(unsigned int j=0;j<fajlovi[i].tabelaSekcija.size();j++){
			fajlovi[i].tabelaSekcija[j].mem.clear();
		}
		fajlovi[i].tabelaSekcija.clear();
	}
	fajlovi.clear();
}

void Ucitavanje::ucitaj(vector<char *> ulazniFajl){
	for(unsigned int j=0;j<ulazniFajl.size();j++){
		ifstream ulazni(ulazniFajl[j]);
		string linija;
		jedanFajl x;
		x.ime= ulazniFajl[j];
		while(getline(ulazni, linija)){
			if(linija.compare("#PocinjeSVE")==0)
				continue;
			vector<string> tokenLinija;
			split(linija, " ,\t\n", tokenLinija);
			
			if(tokenLinija.size()==0)
				continue;
			for(unsigned int i=0;i<tokenLinija.size();i++){
				x.tokeni.push_back(tokenLinija[i]);
			}
		}
		fajlovi.push_back(x);
		ulazni.close();
	}
	napuniLokalMemoriju();
	postaviPocetneLokacije();
	napuniMemoriju();
	razresiSimbole();
	Interpreter::mainLokacija=TabelaSimbola::dohvatiLokacijuSimbola("main");
	oslobodiVektore();
}

void Ucitavanje::split(const string &s, const char* delimit, vector<string> & v){
    char * dup = strdup(s.c_str());
    char * token = strtok(dup, delimit);
    while(token != NULL){
        v.push_back(string(token));
        token = strtok(NULL, delimit);
    }
    free(dup);
}
