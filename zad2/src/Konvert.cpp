#include "Konvert.h"

string Konvert::hexCifre="0123456789ABCDEF";

unsigned int Konvert::konvertujStringToUnInt(string s){
	unsigned int x;   
    stringstream ss;
    ss << std::hex << s;
    ss >> x;
    return x;
}

bool Konvert::proveraHexVrednosti(string s){
	if(s.length()<3)
		return false;
	if(s.substr(0,2).compare("0x")==0){
		for(unsigned int i=2; i<s.length();i++){
			size_t found = hexCifre.find(s[i]);
			if (found==std::string::npos)
				return false; 
		}
		return true;
	}else
		return false;
}

string Konvert::vratiHexNa4B(string s){
	if(s.length()>4)
		s=s.substr(s.length()-4,4);
	if(s.length()==1){
		return "000"+s;
	}
	if(s.length()==2){
		return "00"+s;
	}
	if(s.length()==3){
		return "0"+s;
	}
	if(s.length()==4){
		return s;
	}
	cout<<"konverzija nije validne duzine!ki parametar je predugacak"<<endl;
	throw 1;
}

int Konvert::konvertujStringToInt(string s){
	if(s.length()>4)
		s= vratiHexNa4B(s);
	int x;   
    stringstream ss;
    ss << std::hex << s;
    ss >> x;
    if(x>32767)
		x-=65536;
    return x;
}

string Konvert::intToHex(int i){
	stringstream stream;
	stream << hex << i;
	return velikaSlova(stream.str());
}

string Konvert::intToHexUn(unsigned int i){
	stringstream stream;
	stream << hex << i;
	return velikaSlova(stream.str());
}

string Konvert::hexToBinary8(string s){
	unsigned int i=konvertujStringToUnInt(s);
	bitset<8> b(i);
	return b.to_string();

}

string Konvert::hexToBinary16(string s){
	unsigned int i=konvertujStringToUnInt(s);
	bitset<16> b(i);
	return b.to_string();

}

string Konvert::velikaSlova(string s){
	transform(s.begin(), s.end(),s.begin(), ::toupper);
	return s;
}

string Konvert::hex8ToInstruction(string s){
	string c = hexToBinary8(s).substr(0,5);
	string x = c.substr(0,1);
	c = c.substr(1,4);
	if(c.compare("0000")==0) x += "0";
	if(c.compare("0001")==0) x += "1";
	if(c.compare("0010")==0) x += "2";
	if(c.compare("0011")==0) x += "3";
	if(c.compare("0100")==0) x += "4";
	if(c.compare("0101")==0) x += "5";
	if(c.compare("0110")==0) x += "6";
	if(c.compare("0111")==0) x += "7";
	if(c.compare("1000")==0) x += "8";
	if(c.compare("1001")==0) x += "9";
	if(c.compare("1010")==0) x += "A";
	if(c.compare("1011")==0) x += "B";
	if(c.compare("1100")==0) x += "C";
	if(c.compare("1101")==0) x += "D";
	if(c.compare("1110")==0) x += "E";
	if(c.compare("1111")==0) x += "F";
	return x;
}

string Konvert::binToHex(string c){
	if(c.compare("0000")==0) return "0";
	if(c.compare("0001")==0) return "1";
	if(c.compare("0010")==0) return "2";
	if(c.compare("0011")==0) return "3";
	if(c.compare("0100")==0) return "4";
	if(c.compare("0101")==0) return "5";
	if(c.compare("0110")==0) return "6";
	if(c.compare("0111")==0) return "7";
	if(c.compare("1000")==0) return "8";
	if(c.compare("1001")==0) return "9";
	if(c.compare("1010")==0) return "A";
	if(c.compare("1011")==0) return "B";
	if(c.compare("1100")==0) return "C";
	if(c.compare("1101")==0) return "D";
	if(c.compare("1110")==0) return "E";
	if(c.compare("1111")==0) return "F";
	cout<<"nije moguce pretvoriti ovaj binarni broj u hex!"<<endl;
    throw 1;
}

string Konvert::binaryToHex16(string s){
	string bin=binToHex(s.substr(0,4));
	bin+=binToHex(s.substr(4,4));
	bin+=binToHex(s.substr(8,4));
	bin+=binToHex(s.substr(12,4));
    return bin;
}

string Konvert::hex8ToSize(string s){
	return hexToBinary8(s).substr(5,1);
}

string Konvert::hex8ToAdress(string s){
	return hexToBinary8(s).substr(0,3);
}
