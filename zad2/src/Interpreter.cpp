#include "Interpreter.h"
#include "Konvert.h"
#include "Ucitavanje.h"
#include <unistd.h>
#include <signal.h>
#include <curses.h>

vector<ulazniParametri> Interpreter::nizUlaznihParametara;

string Interpreter::memorija[65536];
string Interpreter::pswZ = "0";
string Interpreter::pswO = "0";
string Interpreter::pswC = "0";
string Interpreter::pswI = "0";
string Interpreter::pswN = "0";
string Interpreter::pswTr = "0";
string Interpreter::pswTl = "0";

int Interpreter::r0 = 0;
int Interpreter::r1 = 0;
int Interpreter::r2 = 0;
int Interpreter::r3 = 0;
int Interpreter::r4 = 0;
int Interpreter::r5 = 0;
int Interpreter::sp = 65281;
int Interpreter::pc = 0;
short Interpreter::rTEMP = 0;
unsigned int Interpreter::mainLokacija = 0;
unsigned int Interpreter::ivTableLokacija = 0;

void Interpreter::pokreni(vector<char *> ulazFajl){
	for(unsigned int i=0;i<nizUlaznihParametara.size();i++){
		if(nizUlaznihParametara[i].ime.compare("iv_table")==0)
			ivTableLokacija=Konvert::konvertujStringToUnInt(nizUlaznihParametara[i].lokacija);
	}
	Ucitavanje::ucitaj(ulazFajl);
	if(ivTableLokacija==0){
		if(proveraSllobodnostiMemorije(0,8)==false){
			ivTableLokacija=vratiSlobodnuLokaciju(8);
		}
	}
	signal(SIGALRM, &sigalrm_handler);
	alarm(1); 
	try{
		izvrsiInstrukciju();
	} catch (int e) {
        cout<<"obradjuje se greska u intrukciji .... "<<e<<endl;
        cout<<"program se zavrsava!"<<endl;
    } 
	cout<<"Uspesno izvrsen program :)"<<endl;
	cout<<"*******************************************************************************"<<endl;
	cout<<"r0: "<<r0<<endl;
	cout<<"r1: "<<r1<<endl;
	cout<<"r2: "<<r2<<endl;
	cout<<"r3: "<<r3<<endl;
	cout<<"r4: "<<r4<<endl;
	cout<<"r5: "<<r5<<endl;
	cout<<"sp: "<<sp<<endl;
	cout<<"pc: "<<pc<<endl;
	cout<<"PSW: "<<toPSW()<<endl;
	cout<<"*******************************************************************************"<<endl;
	cout<<"Ako zelite unesite hex broj memorijske lokacije ciju vrednost zelite da vidite:"<<endl;
	cout<<"Ako zelite da izadjete unesite bilo sta osim numericke vrednosti"<<endl;
	for(;;){
		string lokacija;
		cin>>lokacija;
		if(Konvert::proveraHexVrednosti(lokacija)){
			cout<<memorija[Konvert::konvertujStringToUnInt(lokacija)]<<endl;
		}else{
			break;
		}
	}
	//terminal
	/*initscr();
	cbreak();
	for(;;){
	char x=getch();
	cout<<" ";
	cout<<x<<" ";
	if(x=='a')
		break;
	}
	endwin();*/
}

void Interpreter::sigalrm_handler(int sig){
    cout<<"okinut timer "<<endl;
    string s=memorija[65296]+memorija[65297];
    if(s.compare("")==0){
		alarm(1);
	}else{
		if(s.compare("000")==0)
			alarm(1);
		if(s.compare("001")==0)
			alarm(1);
		if(s.compare("010")==0)
			alarm(1.5);
		if(s.compare("011")==0)
			alarm(2);
		if(s.compare("100")==0)
			alarm(5);
		if(s.compare("101")==0)
			alarm(10);
		if(s.compare("110")==0)
			alarm(30);
		if(s.compare("111")==0)
			alarm(60);
	}
}

string Interpreter::toPSW(){
	return pswI+pswTl+pswTr+"000000000"+pswN+pswC+pswO+pswZ;
}

bool Interpreter::proveraSllobodnostiMemorije(unsigned int i, unsigned int j){
	for(unsigned int o=i;o<i+j+1;o++){
		if(memorija[o].compare("")!=0)
			return true;
	}
	return false;
}

unsigned int Interpreter::vratiSlobodnuLokaciju(unsigned int velicina){
	for(unsigned int i=0;i<60000;i++){
		if(proveraSllobodnostiMemorije(i,velicina))
			return i;
	}
	cout<<"nema memorije"<<endl;
	throw 1;
	return 0;
}


void Interpreter::postaviPSW(string s){
	pswI=dohvatiBitUHex(s, 0);
	pswTl=dohvatiBitUHex(s, 1);
	pswTr=dohvatiBitUHex(s, 2);
	pswN=dohvatiBitUHex(s, 12);
	pswC=dohvatiBitUHex(s, 13);
	pswO=dohvatiBitUHex(s, 14);
	pswZ=dohvatiBitUHex(s, 15);
}

string Interpreter::dohvatiBitUHex(string s, int i){
	s=Konvert::hexToBinary16(s);
	return s.substr(i,1);
}


void Interpreter::dodajNaStek2(string s){
	s=Konvert::vratiHexNa4B(s);
	sp--;
	memorija[sp]=s.substr(2,4);
	sp--;
	memorija[sp]=s.substr(0,2);
}

void Interpreter::dodajNaStek1(string s){
	s=Konvert::vratiHexNa4B(s);
	sp--;
	memorija[sp]=s.substr(2,2);
}

string Interpreter::skiniSaStek2(){
	string temp = memorija[sp];
	sp++;
	temp += memorija[sp];
	sp++;
	return temp;
}

string Interpreter::skiniSaStek1(){
	string temp = memorija[sp];
	sp++;
	return temp;
}

int Interpreter::dohvatiVrednostRegistra(string s){
	if(s.compare("000")==0) return r0;
	if(s.compare("001")==0) return r1;
	if(s.compare("010")==0) return r2;
	if(s.compare("011")==0) return r3;
	if(s.compare("100")==0) return r4;
	if(s.compare("101")==0) return r5;
	if(s.compare("110")==0) return sp;
	if(s.compare("111")==0) return pc;
	return 0;
}

void Interpreter::postaviVrednostRegistra(string s, int i){
	if(s.compare("000")==0) r0=i;
	if(s.compare("001")==0) r1=i;
	if(s.compare("010")==0) r2=i;
	if(s.compare("011")==0) r3=i;
	if(s.compare("100")==0) r4=i;
	if(s.compare("101")==0) r5=i;
	if(s.compare("110")==0) sp=i;
	if(s.compare("111")==0) pc=i;
}

void Interpreter::upisiUMemoriju1(string s, unsigned int lokacija){
	s=Konvert::vratiHexNa4B(s);
	memorija[lokacija]=s.substr(2,2);
}

void Interpreter::upisiUMemoriju2(string s, unsigned int lokacija){
	s=Konvert::vratiHexNa4B(s);
	memorija[lokacija]=s.substr(0,2);
	lokacija++;
	memorija[lokacija]=s.substr(2,2);
}

void Interpreter::postaviPSWN(int i){
	if(i<0){
		pswN="1";
	}else{
		pswN="0";
	}
}
void Interpreter::postaviPSWZ(int i){
	if(i==0){
		pswZ="1";
	}else{
		pswZ="0";
	}
}

void Interpreter::izvrsiInstrukciju(){
	pc=mainLokacija;
	bool izvrsavatiInstrukcije=true;
	string instrukcija, op1, op2;
	unsigned int max=0;
	while(izvrsavatiInstrukcije){
		max++;
		if(max>1000){//limit broja instrukcija uveden u slucaju beskonacne petlje, moze se ukloniti u slucaju validnog koda
			cout<<"dostignut limit instrukcija"<<endl;
			break;
		}
		bool duzina2=true;
		string tempInstr=memorija[pc];
		if(Konvert::hexToBinary8(tempInstr).substr(5,1).compare("0")==0){
			duzina2=false;
		}
		instrukcija = Konvert::hex8ToInstruction(tempInstr);
		pc++;
		if ((instrukcija.compare("1F")==0))//NOP
			continue;
		if ((instrukcija.compare("01") == 0) || (instrukcija.compare("18")  ==  0) || (instrukcija.compare("19") == 0)){
			if (instrukcija.compare("01") == 0){//halt
				izvrsavatiInstrukcije = false;
				cout<<"Izvrsena instrukcija HALT"<<endl;
				break;
			}
			if (instrukcija.compare("18") == 0){//ret
				pc=Konvert::konvertujStringToUnInt(skiniSaStek2());
				cout<<"Izvrsena instrukcija RET"<<endl;
			}
			if (instrukcija.compare("19") == 0){//iret
				postaviPSW(skiniSaStek2());
				pc=Konvert::konvertujStringToUnInt(skiniSaStek2());
				cout<<"Izvrsena instrukcija IRET"<<endl;
			}
			continue;
		}
		string temp = Konvert::hexToBinary8(memorija[pc]);
		pc++;
		string tipAdresiranja=temp.substr(0,3);
		string tipRegistra = temp.substr(4,3);
		string goreDole=temp.substr(7,1);
		if ((instrukcija.compare("03")  ==  0) || (instrukcija.compare("0A") == 0) || (instrukcija.compare("12") == 0) 
			|| (instrukcija.compare("13") == 0) || (instrukcija.compare("14") == 0) || (instrukcija.compare("15") == 0)
			|| (instrukcija.compare("16") == 0) || (instrukcija.compare("17") == 0)){
			
			if(tipAdresiranja.compare("001")==0){//registarsko direktno
				op1=Konvert::intToHex(dohvatiVrednostRegistra(tipRegistra));
				if(duzina2==false){
					op1=Konvert::vratiHexNa4B(op1);
					if(goreDole.compare("1")==0){//H
						op1=op1.substr(0,2);
						
					}else{
						op1=op1.substr(2,2);
					}
				}
			}
			if(tipAdresiranja.compare("010")==0){//registarsko indirektno
				if(dohvatiVrednostRegistra(tipRegistra)<0){
					cout<<"Dogodila se greska! Pristupanje memoriji na nedozvoljenoj lokaciji"<<endl;
					throw -1;
				}
				int vr=dohvatiVrednostRegistra(tipRegistra);
				if(duzina2){
					op1=memorija[vr]+memorija[vr+1];
				}else{
					op1=memorija[vr];
				}
			}
			if(tipAdresiranja.compare("011")==0){//registarsko indirektno sa 8bitnim pomerajem
				int pom=Konvert::konvertujStringToInt(memorija[pc])+dohvatiVrednostRegistra(tipRegistra);
				pc++;
				if (pom<0){
					cout<<"Dogodila se greska! Pristupanje memoriji na nedozvoljenoj lokaciji"<<endl;
					throw -1;
				}
				if(duzina2){
					op1=memorija[pom]+memorija[pom+1];
				}else{
					op1=memorija[pom];
				}
			}
			if(tipAdresiranja.compare("100")==0){//registarsko indirektno sa 16bitnim pomerajem
				int pom=Konvert::konvertujStringToInt(memorija[pc]+memorija[pc+1])+dohvatiVrednostRegistra(tipRegistra);
				pc+=2;
				if (pom<0){
					cout<<"Dogodila se greska! Pristupanje memoriji na nedozvoljenoj lokaciji"<<endl;
					throw -1;
				}
				if(duzina2){
					op1=memorija[pom]+memorija[pom+1];
				}else{
					op1=memorija[pom];
				}
			}
			if(tipAdresiranja.compare("101")==0){//memorijsko
				int pom=Konvert::konvertujStringToInt(memorija[pc]+memorija[pc+1]);
				pc+=2;	
				if(temp.substr(3,1).compare("1")==0){
					if(duzina2==false){
						cout<<"greska velicine operanda adresiranje memorijsko"<<endl;
						throw 1;
					}
					if(pom<0)
						pom=pom;
					op1=Konvert::intToHexUn(pc+pom);
				}else{
					if (pom<0){
						cout<<"Dogodila se greska! Pristupanje memoriji na nedozvoljenoj lokaciji"<<endl;
						throw -1;
					}
					if(duzina2){
						op1=memorija[pom]+memorija[pom+1];
					}else{
						op1=memorija[pom];
					}
				}
			}

			if (instrukcija.compare("03") == 0){//int
				dodajNaStek2(toPSW());
				dodajNaStek2(Konvert::vratiHexNa4B(Konvert::intToHex(dohvatiVrednostRegistra("111"))));
				int pom=(Konvert::konvertujStringToInt(op1) % 8)*2;
				pom+=ivTableLokacija;
				pc=Konvert::konvertujStringToInt(memorija[pom]+memorija[pom+1]);
				cout<<"Izvrsena instrukcija INT"<<endl;
			}
			if (instrukcija.compare("0A") == 0){//not
				op1=Konvert::intToHex(-Konvert::konvertujStringToInt(op1));
				int w=Konvert::konvertujStringToInt(op1);
				postaviPSWN(w);
				postaviPSWZ(w);
				if(tipAdresiranja.compare("001")==0){//registarsko direktno
					if(duzina2==false){
						string t=Konvert::vratiHexNa4B(Konvert::intToHex(dohvatiVrednostRegistra(tipRegistra)));
						if(goreDole.compare("1")==0){//H
							op1=op1+t.substr(2,2);	
						}else{
							op1=t.substr(0,2)+op1;
						}
					}
					postaviVrednostRegistra(tipRegistra, Konvert::konvertujStringToInt(op1));
				}
				if(tipAdresiranja.compare("010")==0){//registarsko indirektno
					if(duzina2){
						upisiUMemoriju2(op1, dohvatiVrednostRegistra(tipRegistra));
					}else{
						upisiUMemoriju1(op1, dohvatiVrednostRegistra(tipRegistra));
					}
				}
				if(tipAdresiranja.compare("011")==0){//registarsko indirektno sa 8bitnim pomerajem
					int i=Konvert::konvertujStringToInt(memorija[pc-1]);
					if(duzina2){
						upisiUMemoriju2(op1, dohvatiVrednostRegistra(tipRegistra)+i);
					}else{
						upisiUMemoriju1(op1, dohvatiVrednostRegistra(tipRegistra)+i);
					}
				}
				if(tipAdresiranja.compare("100")==0){//registarrsko indirektno sa 16bitnim pomerajem
					int i=Konvert::konvertujStringToInt(memorija[pc-2]+memorija[pc-1]);
					if(duzina2){
						upisiUMemoriju2(op1, dohvatiVrednostRegistra(tipRegistra)+i);
					}else{
						upisiUMemoriju1(op1, dohvatiVrednostRegistra(tipRegistra)+i);
					}
				}
				if(tipAdresiranja.compare("101")==0){//memorijsko
					int i=Konvert::konvertujStringToInt(memorija[pc-2]+memorija[pc-1]);
					if(temp.substr(3,1).compare("1")==0){
						i=pc+i;
					}
					if(duzina2){
						upisiUMemoriju2(op1, i);
					}else{
						upisiUMemoriju1(op1, i);
					}
				}
				cout<<"Izvrsena instrukcija NOT"<<endl;
			}
			if (instrukcija.compare("12") == 0){//pop
				if(duzina2==false){
					op1=skiniSaStek1();
				}else{
					op1 = skiniSaStek2();
				}
				if(tipAdresiranja.compare("001")==0){//registarsko direktno
					if(duzina2==false){
						string t=Konvert::vratiHexNa4B(Konvert::intToHex(dohvatiVrednostRegistra(tipRegistra)));
						if(goreDole.compare("1")==0){//H
							op1=op1+t.substr(2,2);	
						}else{
							op1=t.substr(0,2)+op1;
						}
					}
					postaviVrednostRegistra(tipRegistra, Konvert::konvertujStringToInt(op1));
				}
				if(tipAdresiranja.compare("010")==0){//registarsko indirektno
					if(duzina2){
						upisiUMemoriju2(op1, dohvatiVrednostRegistra(tipRegistra));
					}else{
						upisiUMemoriju1(op1, dohvatiVrednostRegistra(tipRegistra));
					}
				}
				if(tipAdresiranja.compare("011")==0){//registarsko indirektno sa 8bitnim pomerajem
					int i=Konvert::konvertujStringToInt(memorija[pc-1]);
					if(duzina2){
						upisiUMemoriju2(op1, dohvatiVrednostRegistra(tipRegistra)+i);
					}else{
						upisiUMemoriju1(op1, dohvatiVrednostRegistra(tipRegistra)+i);
					}
				}
				if(tipAdresiranja.compare("100")==0){//registarrsko indirektno sa 16bitnim pomerajem
					int i=Konvert::konvertujStringToInt(memorija[pc-2]+memorija[pc-1]);
					if(duzina2){
						upisiUMemoriju2(op1, dohvatiVrednostRegistra(tipRegistra)+i);
					}else{
						upisiUMemoriju1(op1, dohvatiVrednostRegistra(tipRegistra)+i);
					}
				}
				if(tipAdresiranja.compare("101")==0){//memorijsko
					int i=Konvert::konvertujStringToInt(memorija[pc-2]+memorija[pc-1]);
					if(temp.substr(3,1).compare("1")==0){
						i=pc+i;
					}
					if(duzina2){
						upisiUMemoriju2(op1, i);
					}else{
						upisiUMemoriju1(op1, i);
					}
				}
				cout<<"Izvrsena instrukcija POP"<<endl;
			}
			if (instrukcija.compare("13") == 0){//jmp
				pc=Konvert::konvertujStringToInt(op1);
				cout<<"Izvrsena instrukcija JMP"<<endl;
			}
			if (instrukcija.compare("14") == 0){//jeq
				if(pswZ.compare("0")==0){
					pc=Konvert::konvertujStringToInt(op1);
				}
				cout<<"Izvrsena instrukcija JEQ"<<endl;
			}
			if (instrukcija.compare("15") == 0){//jne
				if(pswZ.compare("0")!=0){
					pc=Konvert::konvertujStringToInt(op1);
				}
				cout<<"Izvrsena instrukcija JNE"<<endl;
			}
			if (instrukcija.compare("16") == 0){//jgt
				if(pswN.compare("0")==0){
					pc=Konvert::konvertujStringToInt(op1);
				}
				cout<<"Izvrsena instrukcija JGT"<<endl;
			}
			if (instrukcija.compare("17") == 0){//call
				dodajNaStek2(Konvert::intToHexUn(pc));
				pc=Konvert::konvertujStringToInt(op1);
				cout<<"Izvrsena instrukcija CALL"<<endl;
			}
			continue;
		}
		if (instrukcija.compare("11") == 0){//push 
			if (tipAdresiranja.compare("000")==0){//neposredno
				if(duzina2){
					op1=memorija[pc]+memorija[pc+1];
					pc+=2;
				}else{
					op1=memorija[pc];
					pc+=1;
				}	
			}
			if(tipAdresiranja.compare("001")==0){//registarsko direktno
				op1=Konvert::intToHex(dohvatiVrednostRegistra(tipRegistra));
				if(duzina2==false){
					op1=Konvert::vratiHexNa4B(op1);
					if(goreDole.compare("1")==0){//H
						op1=op1.substr(0,2);
						
					}else{
						op1=op1.substr(2,2);
					}
				}
			}
			if(tipAdresiranja.compare("010")==0){//registarsko indirektno
				if(dohvatiVrednostRegistra(tipRegistra)<0){
					cout<<"Dogodila se greska! Pristupanje memoriji na nedozvoljenoj lokaciji"<<endl;
					throw -1;
				}
				int vr=dohvatiVrednostRegistra(tipRegistra);
				if(duzina2){
					op1=memorija[vr]+memorija[vr+1];
				}else{
					op1=memorija[vr];
				}
			}
			if(tipAdresiranja.compare("011")==0){//registarsko indirektno sa 8bitnim pomerajem
				int pom=Konvert::konvertujStringToInt(memorija[pc])+dohvatiVrednostRegistra(tipRegistra);
				pc++;
				if (pom<0){
					cout<<"Dogodila se greska! Pristupanje memoriji na nedozvoljenoj lokaciji"<<endl;
					throw -1;
				}
				if(duzina2){
					op1=memorija[pom]+memorija[pom+1];
				}else{
					op1=memorija[pom];
				}
			}
			if(tipAdresiranja.compare("100")==0){//registarrsko indirektno sa 16bitnim pomerajem
				int pom=Konvert::konvertujStringToInt(memorija[pc]+memorija[pc+1])+dohvatiVrednostRegistra(tipRegistra);
				pc+=2;
				if (pom<0){
					cout<<"Dogodila se greska! Pristupanje memoriji na nedozvoljenoj lokaciji"<<endl;
					throw -1;
				}
				if(duzina2){
					op1=memorija[pom]+memorija[pom+1];
				}else{
					op1=memorija[pom];
				}
			}
			if(tipAdresiranja.compare("101")==0){//memorijsko
				int pom=Konvert::konvertujStringToInt(memorija[pc]+memorija[pc+1]);
				pc+=2;		
				if(temp.substr(3,1).compare("1")==0){
					if(duzina2==false){
						cout<<"greska velicine operanda adresiranje memorijsko"<<endl;
						throw 1;
					}
					if(pom<0)
						pom=pom-1;
					op1=Konvert::intToHexUn(pc+pom);
				}else{
					if (pom<0){
						cout<<"Dogodila se greska! Pristupanje memoriji na nedozvoljenoj lokaciji"<<endl;
						throw -1;
					}
					if(duzina2){
						op1=memorija[pom]+memorija[pom+1];
					}else{
						op1=memorija[pom];
					}
				}
			}
			if(duzina2){
				dodajNaStek2(op1);
			}else{
				dodajNaStek1(op1);
			}
			cout<<"Izvrsena instrukcija PUSH"<<endl;
			continue;
		}
		
		//op1
		int pc1Temp=0;
		string tipAdrTemp=tipAdresiranja;
		string tipRegTemp=tipRegistra;
		if(tipAdresiranja.compare("001")==0){//registarsko direktno
			op1=Konvert::intToHex(dohvatiVrednostRegistra(tipRegistra));
			if(duzina2==false){
				op1=Konvert::vratiHexNa4B(op1);
				if(goreDole.compare("1")==0){//H
					op1=op1.substr(0,2);
					
				}else{
					op1=op1.substr(2,2);
				}
			}
		}
		if(tipAdresiranja.compare("010")==0){//registarsko indirektno
			if(dohvatiVrednostRegistra(tipRegistra)<0){
				cout<<"Dogodila se greska! Pristupanje memoriji na nedozvoljenoj lokaciji"<<endl;
				throw -1;
			}
			int vr=dohvatiVrednostRegistra(tipRegistra);
			if(duzina2){
				op1=memorija[vr]+memorija[vr+1];
			}else{
				op1=memorija[vr];
			}
		}
		if(tipAdresiranja.compare("011")==0){//registarsko indirektno sa 8bitnim pomerajem
			int pom=Konvert::konvertujStringToInt(memorija[pc])+dohvatiVrednostRegistra(tipRegistra);
			pc1Temp=pc;
			pc++;
			if (pom<0){
				cout<<"Dogodila se greska! Pristupanje memoriji na nedozvoljenoj lokaciji"<<endl;
				throw -1;
			}
			if(duzina2){
				op1=memorija[pom]+memorija[pom+1];
			}else{
				op1=memorija[pom];
			}
		}
		if(tipAdresiranja.compare("100")==0){//registarrsko indirektno sa 16bitnim pomerajem
			int pom=Konvert::konvertujStringToInt(memorija[pc]+memorija[pc+1])+dohvatiVrednostRegistra(tipRegistra);
			pc1Temp=pc;
			pc+=2;
			if (pom<0){
				cout<<"Dogodila se greska! Pristupanje memoriji na nedozvoljenoj lokaciji"<<endl;
				throw -1;
			}
			if(duzina2){
				op1=memorija[pom]+memorija[pom+1];
			}else{
				op1=memorija[pom];
			}
		}
		if(tipAdresiranja.compare("101")==0){//memorijsko
			int pom=Konvert::konvertujStringToInt(memorija[pc]+memorija[pc+1]);
			pc1Temp=pc;
			pc+=2;		
			if(temp.substr(3,1).compare("1")==0){
				if(duzina2==false){
					cout<<"greska velicine operanda adresiranje memorijsko"<<endl;
					throw 1;
				}
				if(pom<0)
					pom=pom-1;
				op1=Konvert::intToHexUn(pc+pom);
			}else{
				if (pom<0){
					cout<<"Dogodila se greska! Pristupanje memoriji na nedozvoljenoj lokaciji"<<endl;
					throw -1;
				}
				if(duzina2){
					op1=memorija[pom]+memorija[pom+1];
				}else{
					op1=memorija[pom];
				}
			}
		}
		
		//op2
		temp = Konvert::hexToBinary8(memorija[pc]);
		pc++;
		tipAdresiranja=temp.substr(0,3);
		tipRegistra = temp.substr(4,3);
		goreDole=temp.substr(7,1);
		if (tipAdresiranja.compare("000")==0){//neposredno
			if(duzina2){
				op2=memorija[pc]+memorija[pc+1];
				pc+=2;
			}else{
				op2=memorija[pc];
				pc+=1;
			}
		}
		if(tipAdresiranja.compare("001")==0){//registarsko direktno
			op2=Konvert::intToHex(dohvatiVrednostRegistra(tipRegistra));
			if(duzina2==false){
				op2=Konvert::vratiHexNa4B(op1);
				if(goreDole.compare("1")==0){//H
					op2=op2.substr(0,2);
					
				}else{
					op2=op2.substr(2,2);
				}
			}
		}
		if(tipAdresiranja.compare("010")==0){//registarsko indirektno
			if(dohvatiVrednostRegistra(tipRegistra)<0){
				cout<<"Dogodila se greska! Pristupanje memoriji na nedozvoljenoj lokaciji"<<endl;
				throw -1;
			}
			int vr=dohvatiVrednostRegistra(tipRegistra);
			if(duzina2){
				op2=memorija[vr]+memorija[vr+1];
			}else{
				op2=memorija[vr];
			}
		}
		if(tipAdresiranja.compare("011")==0){//registarsko indirektno sa 8bitnim pomerajem
			int pom=Konvert::konvertujStringToInt(memorija[pc])+dohvatiVrednostRegistra(tipRegistra);
			pc++;
			if (pom<0){
				cout<<"Dogodila se greska! Pristupanje memoriji na nedozvoljenoj lokaciji"<<endl;
				throw -1;
			}
			if(duzina2){
				op2=memorija[pom]+memorija[pom+1];
			}else{
				op2=memorija[pom];
			}
		}
		if(tipAdresiranja.compare("100")==0){//registarrsko indirektno sa 16bitnim pomerajem
			int pom=Konvert::konvertujStringToInt(memorija[pc]+memorija[pc+1])+dohvatiVrednostRegistra(tipRegistra);
			pc+=2;
			if (pom<0){
				cout<<"Dogodila se greska! Pristupanje memoriji na nedozvoljenoj lokaciji"<<endl;
				throw -1;
			}
			if(duzina2){
				op2=memorija[pom]+memorija[pom+1];
			}else{
				op2=memorija[pom];
			}
		}
		if(tipAdresiranja.compare("101")==0){//memorijsko
			int pom=Konvert::konvertujStringToInt(memorija[pc]+memorija[pc+1]);
			pc+=2;		
			if(temp.substr(3,1).compare("1")==0){
				if(duzina2==false){
					cout<<"greska velicine operanda adresiranje memorijsko"<<endl;
					throw 1;
				}
				if(pom<0)
					pom=pom-1;
				op2=Konvert::intToHexUn(pc+pom);
			}else{
				if (pom<0){
					cout<<"Dogodila se greska! Pristupanje memoriji na nedozvoljenoj lokaciji"<<endl;
					throw -1;
				}
				if(duzina2){
					op2=memorija[pom]+memorija[pom+1];
				}else{
					op2=memorija[pom];
				}
			}
		}
		int x1=Konvert::konvertujStringToInt(op1);
		int x2=Konvert::konvertujStringToInt(op2);
		if (instrukcija.compare("02") == 0){//xchg
			string t=op1;
			op1=op2;
			op2=t;
			if(tipAdresiranja.compare("000")==0){//neposredno
				cout<<"greska adresiranja instrukcija xchg"<<endl;
				throw 1;
			}
			if(tipAdresiranja.compare("001")==0){//registarsko direktno
				if(duzina2==false){
					string t=Konvert::vratiHexNa4B(Konvert::intToHex(dohvatiVrednostRegistra(tipRegistra)));
					if(goreDole.compare("1")==0){//H
						op2=op2+t.substr(2,2);	
					}else{
						op2=t.substr(0,2)+op2;
					}
				}
				postaviVrednostRegistra(tipRegistra, Konvert::konvertujStringToInt(op2));
			}
			if(tipAdresiranja.compare("010")==0){//registarsko indirektno
				if(duzina2){
					upisiUMemoriju2(op2, dohvatiVrednostRegistra(tipRegistra));
				}else{
					upisiUMemoriju1(op2, dohvatiVrednostRegistra(tipRegistra));
				}
			}
			if(tipAdresiranja.compare("011")==0){//registarsko indirektno sa 8bitnim pomerajem
				int i=Konvert::konvertujStringToInt(memorija[pc-1]);
				if(duzina2){
					upisiUMemoriju2(op2, dohvatiVrednostRegistra(tipRegistra)+i);
				}else{
					upisiUMemoriju1(op2, dohvatiVrednostRegistra(tipRegistra)+i);
				}
			}
			if(tipAdresiranja.compare("100")==0){//registarrsko indirektno sa 16bitnim pomerajem
				int i=Konvert::konvertujStringToInt(memorija[pc-2]+memorija[pc-1]);
				if(duzina2){
					upisiUMemoriju2(op2, dohvatiVrednostRegistra(tipRegistra)+i);
				}else{
					upisiUMemoriju1(op2, dohvatiVrednostRegistra(tipRegistra)+i);
				}
			}
			if(tipAdresiranja.compare("101")==0){//memorijsko
				int i=Konvert::konvertujStringToInt(memorija[pc-2]+memorija[pc-1]);
				if(duzina2){
					upisiUMemoriju2(op2, i);
				}else{
					upisiUMemoriju1(op2, i);
				}
			}
				
			cout<<"Izvrsena instrukcija XCHG"<<endl;
		}
		if (instrukcija.compare("04") == 0){//mov
			op1=op2;
			cout<<"Izvrsena instrukcija MOV"<<endl;
		}
		if (instrukcija.compare("05") == 0){//add
			op1=Konvert::intToHex((short)(x1+x2));
			short t= x1+x2;
			if(((x1<0)&&(x2<0)&&(t>0))||((x1>0)&&(x2>0)&&(t<0))){
				pswO="1";
				pswC="1";
			}else{
				pswO="0";
				pswC="0";
			}
			cout<<"Izvrsena instrukcija ADD"<<endl;
		}
		if (instrukcija.compare("06") == 0){//sub
			op1=Konvert::intToHex((short)(x1-x2));
			short t=x1-x2;
			if(((x1<0)&&(x2>0)&&(t>0))||((x1>0)&&(x2<0)&&(t<0))){
				pswO="1";
				pswC="1";
			}else{
				pswO="0";
				pswC="0";
			}
			cout<<"Izvrsena instrukcija SUB"<<endl;
		}
		if (instrukcija.compare("07") == 0){//mul
			op1=Konvert::intToHex((short)(x1*x2));
			cout<<"Izvrsena instrukcija MUL"<<endl;
		}
		if (instrukcija.compare("08") == 0){//div
			op1=Konvert::intToHex((short)(x1/x2));
			cout<<"Izvrsena instrukcija DIV"<<endl;
		}
		if (instrukcija.compare("09") == 0){//cmp
			int x1=Konvert::konvertujStringToInt(op1);
			int x2=Konvert::konvertujStringToInt(op2);
			rTEMP=x1-x2;
			if(rTEMP==0){
				pswZ="1";
			}else{
				pswZ="0";
			}
			if(rTEMP<0){
				pswN="1";
			}else{
				pswN="0";
			}
			if(((x1<0)&&(x2>0)&&(rTEMP>0))||((x1>0)&&(x2<0)&&(rTEMP<0))){
				pswO="1";
				pswC="1";
			}else{
				pswO="0";
				pswC="0";
			}
			cout<<"Izvrsena instrukcija CMP"<<endl;
			continue;
		}
		if (instrukcija.compare("0B") == 0){//and
			op1=Konvert::intToHex((short)(x1&x2));
			cout<<"Izvrsena instrukcija AND"<<endl;
		}
		if (instrukcija.compare("0C") == 0){//or
			op1=Konvert::intToHex((short)(x1|x2));
			cout<<"Izvrsena instrukcija OR"<<endl;
		}
		if (instrukcija.compare("0D") == 0){//xor
			op1=Konvert::intToHex((short)(x1^x2));
			cout<<"Izvrsena instrukcija XOR"<<endl;
		}
		if (instrukcija.compare("0E") == 0){//test
			int x1=Konvert::konvertujStringToInt(op1);
			int x2=Konvert::konvertujStringToInt(op2);
			rTEMP=x1&x2;
			if(rTEMP==0){
				pswZ="1";
			}else{
				pswZ="0";
			}
			if(rTEMP<0){
				pswN="1";
			}else{
				pswN="0";
			}
			cout<<"Izvrsena instrukcija TEST"<<endl;
			continue;
		}
		if (instrukcija.compare("0F") == 0){//shl
			op1=Konvert::intToHex((short)(x1<<x2));
			string t=Konvert::hexToBinary16(Konvert::intToHex(x1));
			for(int i=0;i<x2;i++){
				if(t.substr(i,1).compare("1")==0){
					pswC="1";
					break;
				}
				pswC="0";
			}
			cout<<"Izvrsena instrukcija SHL"<<endl;
		}
		if (instrukcija.compare("10") == 0){//shr
			op1=Konvert::intToHex((short)(x1>>x2));
			string t=Konvert::hexToBinary16(Konvert::intToHex(x1));
			for(int i=0;i<x2;i++){
				if(t.substr(t.length()-i,1).compare("1")==0){
					pswC="1";
					break;
				}
				pswC="0";
			}
			cout<<"Izvrsena instrukcija SHR"<<endl;
		}	
		if(Konvert::konvertujStringToInt(op1)==0){
			pswZ="1";
		}else{
			pswZ="0";
		}
		if(Konvert::konvertujStringToInt(op1)<0){
			pswN="1";
		}else{
			pswN="0";
		}
		if(tipAdrTemp.compare("001")==0){//registarsko direktno
			if(duzina2==false){
				string t=Konvert::vratiHexNa4B(Konvert::intToHex(dohvatiVrednostRegistra(tipRegistra)));
				if(goreDole.compare("1")==0){//H
					op1=op1+t.substr(2,2);	
				}else{
					op1=t.substr(0,2)+op1;
				}
			}
			postaviVrednostRegistra(tipRegTemp, Konvert::konvertujStringToInt(op1));
		}
		if(tipAdrTemp.compare("010")==0){//registarsko indirektno
			if(duzina2){
				upisiUMemoriju2(op1, dohvatiVrednostRegistra(tipRegTemp));
			}else{
				upisiUMemoriju1(op1, dohvatiVrednostRegistra(tipRegTemp));
			}
		}
		if(tipAdrTemp.compare("011")==0){//registarsko indirektno sa 8bitnim pomerajem
			int i=Konvert::konvertujStringToInt(memorija[pc1Temp]);
			if(duzina2){
				upisiUMemoriju2(op1, dohvatiVrednostRegistra(tipRegTemp)+i);
			}else{
				upisiUMemoriju1(op1, dohvatiVrednostRegistra(tipRegTemp)+i);
			}
		}
		if(tipAdrTemp.compare("100")==0){//registarrsko indirektno sa 16bitnim pomerajem
			int i=Konvert::konvertujStringToInt(memorija[pc1Temp]+memorija[pc1Temp+1]);
			upisiUMemoriju2(op1, dohvatiVrednostRegistra(tipRegTemp)+i);
		}
		if(tipAdrTemp.compare("101")==0){//memorijsko
			int i=Konvert::konvertujStringToInt(memorija[pc1Temp]+memorija[pc1Temp+1]);
			if(temp.substr(3,1).compare("1")==0){
				i=pc1Temp+2+i;
			}
			if(duzina2){
				upisiUMemoriju2(op1, i);
			}else{
				upisiUMemoriju1(op1, i);
			}
		}
	}
}
