#ifndef __Konvert_H_
#define __Konvert_H_

#include <iostream>
#include <cstring>
#include <sstream>
#include <bitset>
#include <algorithm>
using namespace std;

class Konvert {
	private:
		static string hexCifre;
	public:
		static unsigned int konvertujStringToUnInt(string s);
		static int konvertujStringToInt(string s);
		static string hexToBinary8(string s);
		static string hexToBinary16(string s);
		static string hex8ToInstruction(string s);
		static string hex8ToSize(string s);
		static string hex8ToAdress(string s);
		static string binaryToHex16(string s);
		static string binToHex(string c);
		static string intToHex(int i);
		static string intToHexUn(unsigned int i);
		static string velikaSlova(string s);
		static bool proveraHexVrednosti(string s);
		static string vratiHexNa4B(string s);
};

#endif



