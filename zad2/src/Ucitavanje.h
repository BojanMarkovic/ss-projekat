#ifndef __Ucitavanje_H_
#define __Ucitavanje_H_

#include <iostream>
#include <cstring>
#include <vector>
#include <fstream>
#include <cstdlib>
using namespace std;

typedef struct{
	vector<string> mem;
	string ime;
	string flegovi;
	unsigned int krajnjaLokacija;
	unsigned int pocetnaLokacija;
	bool odradjen;
}sekcijeT;

typedef struct{
	vector<string> tokeni;
	vector<sekcijeT> tabelaSekcija;
	string ime;
}jedanFajl;

class Ucitavanje {
	private:
		static unsigned int defaultLokacija;
		static string trenutnaLokacija;
		static string trenutnaLokacijaSekcija;
	public:
		static unsigned int lokacijaTabeleSimbola;
		static void napuniLokalMemoriju();
		static vector<jedanFajl> fajlovi;
		static void ucitaj(vector<char *> ulazniFajl);
		static void split(const string &s, const char* delimit, vector<string> & v);
		static void razresiSimbole();
		static void postaviPocetneLokacije();
		static void napuniMemoriju();
		static unsigned int nadjiNajvisu();
		static void oslobodiVektore();
		static unsigned int dohvatiLokacijuSekcije(string sekcija, string fajl);
};

#endif




