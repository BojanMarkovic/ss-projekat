#include <iostream>
#include <cstring>
#include "Interpreter.h"
#include "Konvert.h"
using namespace std;

int main(int argc, char *argv[]) {
	try {
		vector<char *> ulaz;
		for(int i=1;i<argc;i++){
			string p= argv[i];
			if(p.substr(0,7).compare("-place=")==0){
				string sekcija="";
				for(unsigned int j=0;j<p.length()-7;j++){
					if(p.substr(j+7,1).compare("@")!=0){
						sekcija+=p.substr(j+7,1);
					}else{
						break;
					}
				}
				ulazniParametri x;
				x.ime=sekcija;
				x.lokacija=p.substr(8+sekcija.length(),6);
				if((Konvert::konvertujStringToUnInt(x.lokacija)<0)||(
				Konvert::konvertujStringToUnInt(x.lokacija)>Konvert::konvertujStringToUnInt("F000"))){
					cout<<"lokacija nije validna "<<x.lokacija<<endl;
					throw 1;
				}
				Interpreter::nizUlaznihParametara.push_back(x);		
				continue;
			}else{
				string in = p;
				char *ulazFajl = new char[in.length() + 1];
				strcpy(ulazFajl, in.c_str());
				ulaz.push_back(ulazFajl);
			}
		}
        cout << "Pocinje rad Emulatora!" << endl;
        Interpreter::pokreni(ulaz);
        return 0;
    } catch (int e) {
        cout << "Dogodila se greska broj" <<e<< endl;
    } 
}
