#include "Instrukcije.h"
#include "Prolazi.h"
#include "Konvert.h"
#include "Registri.h"
#include "TabelaSimbola.h"
#include <vector>


string Instrukcije::nizImena[]={"halt", "xchg", "int", "mov", "add", 
		"sub", "mul", "div", "cmp", "not", "and", "or", "xor", "test", 
		"shl", "shr", "push", "pop", "jmp", "jeq", "jne", "jgt", "call", 
		"ret", "iret"};
		
string Instrukcije::nizSifara[]={"01", "02", "03", "04", "05", "06", 
		"07", "08", "09", "0A", "0B", "0C", "0D", "0E", "0F", "10", "11", 
		"12", "13", "14", "15", "16", "17", "18", "19"};
		
bool Instrukcije::pronadji(string s){
	for(int i=0;i<25;i++)
		if(s.compare(nizImena[i])==0)
			return true;
	return false;
}

string Instrukcije::vratiHex(string s){
	for(int i=0;i<25;i++)
		if(s.compare(nizImena[i])==0)
			return nizSifara[i];
	return NULL;
}

unsigned int Instrukcije::izracunajBrojac(string s){
	vector<string> tokenLinija;
	Prolazi::split(s, " ,\t\n", tokenLinija);
	if(tokenLinija.size()==0)
		return 0;
	
	string inst=tokenLinija[0];
	bool uradi = false;
	if ((inst.compare(".align") != 0) && (inst.compare(".skip") != 0) && 
		(inst.compare(".byte") != 0) && (inst.compare(".word") != 0)){
		if (inst.length() > 4) {
			if (inst.substr(inst.length()-1, 1).compare("b") == 0)
				uradi = true;
			inst.erase(inst.length()-1, 1);
		}else if (inst.length() > 3) {
			if (inst.substr(inst.length()-1, 1).compare("b") == 0){
				uradi = true;
				inst.erase(inst.length()-1, 1);
			}
			if (inst.substr(inst.length()-1, 1).compare("w") == 0)
				inst.erase(inst.length()-1, 1);
		}
	}
	if((inst.compare("halt")==0) || (inst.compare("ret")==0) || (inst.compare("iret")==0)){
		if(tokenLinija.size()>1){
			cout<<"greska u broju parametara za instrukciju"<<endl;
			throw 1;
		}
		return 1;
	}
	string op1=tokenLinija[1];
	if((inst.compare("not")==0) || (inst.compare("int")==0) || (inst.compare("pop")==0)
		 || (inst.compare("jne")==0) || (inst.compare("jeq")==0) || (inst.compare("call")==0)
		 || (inst.compare("jmp")==0) || (inst.compare("jgt")==0)){
		if(tokenLinija.size()>2){
			cout<<"greska u broju parametara za instrukciju"<<endl;
			throw 1;
		}
		return dstVrednost(op1, uradi);
	}
	
	if((inst.compare("push")==0)){
		if(tokenLinija.size()>2){
			cout<<"greska u broju parametara za instrukciju"<<endl;
			throw 1;
		}
		return 1+srcVrednost(op1, uradi);
	}
	if(tokenLinija.size()!=3){
		cout<<"greska u broju parametara za instrukciju"<<endl;
		throw 1;
	}
	unsigned int povratnaVrednost=0;
	povratnaVrednost+=dstVrednost(op1, uradi);
	povratnaVrednost+=srcVrednost(tokenLinija[2], uradi);
	return povratnaVrednost;
}

unsigned int Instrukcije::dstVrednost(string s, bool uradi){
	if(Konvert::proveraHexVrednosti(s)){
		cout<<"greska u nacinu adresiranja"<<endl;
		throw 1;
	}
	if(s.find('&')!=string::npos){
		cout<<"greska u nacinu adresiranja"<<endl;
		throw 1;
	}
	if((s.find('*')!=string::npos) || (s.find('$')!=string::npos)){
		if((s.find('*')!=string::npos)){
			if(!Konvert::proveraHexVrednosti(s.erase(0,1))){
				cout<<"greska u nacinu adresiranja"<<endl;
				throw 1;
			}
			if(uradi)
				return 3;
		}
		return 4;
	}
	if(Registri::pronadji(s.substr(0,2))){
		if(s.length()==2)
			return 2;
		if(s.substr(2,1).compare("[")!=0){
			if(s.length()==3){
				if((s.substr(2,1).compare("l")==0)||(s.substr(2,1).compare("h")==0))
					return 1;
			}
			cout<<"registar nije prepoznat"<<endl;
			throw 1;
		}
	}
	if(Registri::pronadji(s.substr(0,2))){
		s.erase(0,2);
		if((s.substr(0,1).compare("[")==0)&&(s.substr(s.length()-1,1).compare("]")==0)){;
			if (Konvert::proveraHexVrednosti(s.substr(1, s.length() - 2)))
				if(uradi)
					return 3;
			return 4;
		}
		cout<<"greska u nacinu adresiranja"<<endl;
		throw 1;
	}
	return  4;
}

unsigned int Instrukcije::srcVrednost(string s, bool uradi){
	if(Konvert::proveraHexVrednosti(s)){
		if(uradi)
			return 2;
		return 3;
	}
	if(s.find('&')!=string::npos)
		return 3;
	if((s.find('*')!=string::npos) || (s.find('$')!=string::npos)){
		if((s.find('*')!=string::npos)){
			if(!Konvert::proveraHexVrednosti(s.erase(0,1))){
				cout<<"greska u nacinu adresiranja"<<endl;
				throw 1;
			}
			if(uradi)
				return 2;
		}
		return 3;
	}
	if(Registri::pronadji(s.substr(0,2))){
		if(s.length()==2)
			return 1;
		if(s.substr(2,1).compare("[")!=0){
			if(s.length()==3){
				if((s.substr(2,1).compare("l")==0)||(s.substr(2,1).compare("h")==0))
					return 1;
			}
			cout<<"registar nije prepoznat"<<endl;
			throw 1;
		}
	}
	if(Registri::pronadji(s.substr(0,2))){
		s.erase(0,2);
		if((s.substr(0,1).compare("[")==0)&&(s.substr(s.length()-1,1).compare("]")==0)){
			if (Konvert::proveraHexVrednosti(s.substr(1, s.length() - 2)))
				if(uradi)
					return 2;
			return 3;
		}
		cout<<"greska u nacinu adresiranja"<<endl;
		throw 1;
	}
	return 3;
}
