#ifndef __Konvert_H_ 
#define __Konvert_H_
#include <iostream>
#include <cstring>
#include <sstream>
#include <bitset>
#include <algorithm>

using namespace std;

class Konvert{
	private:
		static string hexCifre;
		static string binToHex(string c);
	public:
		static unsigned int konvertujStringToUnInt(string &s);
		static int konvertujStringToInt(string &s);
		static bool proveraHexVrednosti(string s);
		static bool broj(const std::string& s);
		static string konvertujUnIntToHex(unsigned int i);
		static string konvertujIntToHex(int i);
		static string velikaSlova(string s);
		static string hexToBinary5(string s);
		static string binaryToHex8(string s);
		static string vratiHexNa4BInt(string s);
		static string vratiHexNa4B(string s);
		static string vratiHexNa2B(string s);
};

#endif
