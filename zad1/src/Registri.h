#ifndef __Registri_H_
#define __Registri_H_

#include <iostream>
#include <cstring>

using namespace std;

class Registri {
	private:
		static string nizRegistara[];
		static string nizSifara[];
	public:
		static bool pronadji(string s);
		static string vratiHex(string s);
};

#endif 

