#ifndef __Instrukcije_H_ 
#define __Instrukcije_H_
#include <iostream>
#include <cstring>
using namespace std;

class Instrukcije{
	private:
		static string nizImena[];
		static string nizSifara[];
		static unsigned int dstVrednost(string s, bool uradi);
		static unsigned int srcVrednost(string s,  bool uradi);
	public:
		static bool pronadji(string s);
		static string vratiHex(string s);
		static unsigned int izracunajBrojac(string s);
};

#endif 
