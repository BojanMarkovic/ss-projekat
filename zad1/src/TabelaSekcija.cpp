#include "TabelaSekcija.h"
#include "Instrukcije.h"
#include "TabelaSimbola.h"
#include "Konvert.h"
#include "Prolazi.h"
#include "Registri.h"
#include "TabelaRelokacije.h"
#include <fstream>

vector<TabelaSekcija> TabelaSekcija::nizTabelaSekcija;

void TabelaSekcija::init() {
    TabelaSekcija("data", "PRW");
    TabelaSekcija("text", "PXR");
    TabelaSekcija("bss", "RW");
}

TabelaSekcija::TabelaSekcija(string i, string f) {
    if ((f.compare("PRW") == 0) || (f.compare("PXR") == 0) || (f.compare("RW") == 0)) {
        ime = i;
        flegovi = f;
        brojac = 0;
        velicina = 0;
        nizTabelaSekcija.push_back(TabelaSekcija(this));
    } else {
        cout << "flegovi za sekciju nisu validni " << i << endl;
        throw 1;
    }
}

TabelaSekcija::TabelaSekcija(TabelaSekcija *t) {
    ime = t->ime;
    flegovi = t->flegovi;
    brojac = t->brojac;
    velicina = t->velicina;
}

void TabelaSekcija::resetBrojac() {
    brojac = 0;
}

void TabelaSekcija::dodajLiniju(string s, string t) {
    for (unsigned int i = 0; i < nizTabelaSekcija.size(); i++) {
        if (nizTabelaSekcija[i].getIme().compare(t) == 0) {
            if (nizTabelaSekcija[i].flegovi.compare("RW") != 0)
                nizTabelaSekcija[i].tokeniSekcije.push_back(s);
            return;
        }
    }
}

void TabelaSekcija::uvecajBrojac(unsigned int j, string t) {
    for (unsigned int i = 0; i < nizTabelaSekcija.size(); i++) {
        if (nizTabelaSekcija[i].getIme().compare(t) == 0) {
            nizTabelaSekcija[i].brojac += j;
            nizTabelaSekcija[i].velicina = nizTabelaSekcija[i].brojac;
        }
    }
}

bool TabelaSekcija::proveraBssTip(string t) {
    for (unsigned int i = 0; i < nizTabelaSekcija.size(); i++) {
        if (nizTabelaSekcija[i].getIme().compare(t) == 0) {
            if (nizTabelaSekcija[i].flegovi.compare("RW") == 0)
                return true;
        }
    }
    return false;
}

unsigned int TabelaSekcija::getBrojac() {
    return brojac;
}

string TabelaSekcija::getIme() {
    return ime;
}

string TabelaSekcija::getFlegovi() {
    return flegovi;
}

unsigned int TabelaSekcija::vratiBrojac(string t) {
    for (unsigned int i = 0; i < nizTabelaSekcija.size(); i++) {
        if (nizTabelaSekcija[i].getIme().compare(t) == 0) {
            return nizTabelaSekcija[i].getBrojac();
        }
    }
    cout << "nema sekcije " << t << endl;
    throw 1;
}

void TabelaSekcija::razresiSe() {
    for (unsigned int i = 0; i < nizTabelaSekcija.size(); i++) {
        if (nizTabelaSekcija[i].flegovi.compare("PRW") == 0)
            razresiSePRW(nizTabelaSekcija[i]);
        if (nizTabelaSekcija[i].flegovi.compare("PXR") == 0)
            razresiSePXR(nizTabelaSekcija[i]);
        //za RW samo velicinu procitas
    }
}

void TabelaSekcija::razresiSePRW(TabelaSekcija &t) {
    t.brojac = 0;
    for (unsigned int i = 0; i < t.tokeniSekcije.size(); i++) {
        vector<string> tokenLinija;
        Prolazi::split(t.tokeniSekcije[i], " ,\t\n", tokenLinija);
        if (tokenLinija.size() == 0)
            continue;
        if (Instrukcije::pronadji(tokenLinija[0])) {
            cout << "nije instrukcija " << tokenLinija[0] << endl;
            throw 1;
        }
        if (tokenLinija.size() > 2) {
            cout << "previse argumenata u liniji " << t.tokeniSekcije[i] << endl;
            throw 1;
        }
        string s = tokenLinija[1];
        if (tokenLinija[0].compare(".byte") == 0) {
            if (Konvert::proveraHexVrednosti(s)) {
                izlazPRW x;
                s.erase(0, 2);
                if (s.length() == 1) {
                    x.ofset = t.brojac;
                    x.sadrzajHex = "0" + s;
                    t.nizPodataka.push_back(x);
                    t.brojac++;
                    continue;
                }
                if (s.length() == 2) {
                    x.ofset = t.brojac;
                    x.sadrzajHex = s;
                    t.nizPodataka.push_back(x);
                    t.brojac++;
                    continue;
                }
                cout << "hex vrednost nije validne duzine " << t.tokeniSekcije[i] << endl;
                throw 1;
            } else if (TabelaSimbola::pronadji(s)) {
                izlazPRW x;
                if (TabelaSimbola::checkLokal(s)) {
                    TabelaRelokacije::dodaj(t.ime, "R_386_16",
                                            TabelaSimbola::vratiBR(s),
                                            Konvert::konvertujUnIntToHex(t.brojac));
                    string b = Konvert::vratiHexNa2B(Konvert::
                                                     konvertujUnIntToHex(TabelaSimbola::getOfset(s)));
                    x.ofset = t.brojac;
                    x.sadrzajHex = b.substr(0, 2);
                    t.nizPodataka.push_back(x);
                    t.brojac++;
                } else {
                    TabelaRelokacije::dodaj(t.ime, "R_386_16",
                                            TabelaSimbola::vratiBR(s),
                                            Konvert::konvertujUnIntToHex(t.brojac));
                    x.ofset = t.brojac;
                    x.sadrzajHex = "00";
                    t.nizPodataka.push_back(x);
                    t.brojac++;
                }
            } else {
                cout << "nije hex vrednost a ni simbol " << t.tokeniSekcije[i] << endl;
                throw 1;
            }
            continue;
        }
        if (tokenLinija[0].compare(".word") == 0) {
            if (Konvert::proveraHexVrednosti(s)) {
                izlazPRW x;
                s.erase(0, 2);
                if (s.length() == 1) {
                    x.ofset = t.brojac;
                    x.sadrzajHex = "0" + s;
                    t.nizPodataka.push_back(x);
                    t.brojac++;
                    continue;
                }
                if (s.length() == 2) {
                    x.ofset = t.brojac;
                    x.sadrzajHex = s;
                    t.nizPodataka.push_back(x);
                    t.brojac++;
                    continue;
                }
                if (s.length() == 3) {
                    x.ofset = t.brojac;
                    x.sadrzajHex = "0" + s[0];
                    t.nizPodataka.push_back(x);
                    t.brojac++;
                    x.ofset = t.brojac;
                    x.sadrzajHex = s.substr(1, 2);
                    t.nizPodataka.push_back(x);
                    t.brojac++;
                    continue;
                }
                if (s.length() == 4) {
                    x.ofset = t.brojac;
                    x.sadrzajHex = s.substr(0, 2);
                    t.nizPodataka.push_back(x);
                    t.brojac++;
                    x.ofset = t.brojac;
                    x.sadrzajHex = s.substr(2, 2);
                    t.nizPodataka.push_back(x);
                    t.brojac++;
                    continue;
                }
            } else if (TabelaSimbola::pronadji(s)) {
                izlazPRW x;
                if (TabelaSimbola::checkLokal(s)) {
                    TabelaRelokacije::dodaj(t.ime, "R_386_16",
                                            TabelaSimbola::vratiBR(s),
                                            Konvert::konvertujUnIntToHex(t.brojac));
                    string b = Konvert::vratiHexNa4B(Konvert::
                                                     konvertujUnIntToHex(TabelaSimbola::getOfset(s)));
                    x.ofset = t.brojac;
                    x.sadrzajHex = b.substr(0, 2);
                    t.nizPodataka.push_back(x);
                    t.brojac++;
                    x.ofset = t.brojac;
                    t.brojac++;
                    x.sadrzajHex = b.substr(2, 2);
                    t.nizPodataka.push_back(x);
                } else {
                    TabelaRelokacije::dodaj(t.ime, "R_386_16",
                                            TabelaSimbola::vratiBR(s),
                                            Konvert::konvertujUnIntToHex(t.brojac));
                    x.ofset = t.brojac;
                    x.sadrzajHex = "00";
                    t.nizPodataka.push_back(x);
                    t.brojac++;
                    x.ofset = t.brojac;
                    t.brojac++;
                    t.nizPodataka.push_back(x);
                }
            } else {
                cout << "parametri za .word direktivu nisu validni " << t.tokeniSekcije[i] << endl;
                throw 1;
            }
            continue;
        }
        if (tokenLinija[0].compare(".align") == 0) {
            if (Konvert::proveraHexVrednosti(s)) {
                unsigned int p = Konvert::konvertujStringToUnInt(s);
                for (unsigned int q = 0; q < p; q += 2) {
                    izlazPRW x;
                    x.ofset = t.brojac;
                    x.sadrzajHex = "00";
                    t.nizPodataka.push_back(x);
                    t.brojac++;
                }
            } else {
                cout << "nije hex vrednost " << t.tokeniSekcije[i] << endl;
                throw 1;
            }
            continue;
        }
        if (tokenLinija[0].compare(".skip") == 0) {
            if (Konvert::proveraHexVrednosti(s)) {
                unsigned int p = Konvert::konvertujStringToUnInt(s);
                for (unsigned int q = 0; q < p; q += 2) {
                    izlazPRW x;
                    x.ofset = t.brojac;
                    x.sadrzajHex = "00";
                    t.nizPodataka.push_back(x);
                    t.brojac++;
                }
            } else {
                cout << "nije hex vrednost " << t.tokeniSekcije[i] << endl;
                throw 1;
            }
            continue;
        }
    }
}

void TabelaSekcija::razresiSePXR(TabelaSekcija &t) {
	t.brojac = 0;
    for (unsigned int q = 0; q < t.tokeniSekcije.size(); q++) {
        vector<string> tokenLinija;
        Prolazi::split(t.tokeniSekcije[q], " ,\t\n", tokenLinija);
        if (tokenLinija.size() == 0)
            continue;
        string inst = tokenLinija[0];
        bool uradi = false;
        if ((inst.compare(".align") != 0) && (inst.compare(".skip") != 0) && 
			(inst.compare(".byte") != 0) && (inst.compare(".word") != 0)){
			if (inst.length() > 4) {
				if (inst.substr(inst.length()-1, 1).compare("b") == 0)
					uradi = true;
				inst.erase(inst.length()-1, 1);
			}else if (inst.length() > 3) {
				if (inst.substr(inst.length()-1, 1).compare("b") == 0){
					uradi = true;
					inst.erase(inst.length()-1, 1);
				}
				if (inst.substr(inst.length()-1, 1).compare("w") == 0)
					inst.erase(inst.length()-1, 1);
			}
		}
        string kodiran = "";
        if ((inst.compare("halt") == 0) || (inst.compare("ret") == 0) || (inst.compare("iret") == 0)) {
            kodiran = Konvert::hexToBinary5(Instrukcije::vratiHex(inst));
            kodiran += "000";
            t.kodovanInstr.push_back(Konvert::binaryToHex8(kodiran) + " ");
            t.brojac++;
            continue;
        }
        string op1 = tokenLinija[1];
        if ((inst.compare("not") == 0) || (inst.compare("int") == 0) || (inst.compare("pop") == 0)
            || (inst.compare("jne") == 0) || (inst.compare("jeq") == 0) || (inst.compare("call") == 0)
            || (inst.compare("jmp") == 0)|| (inst.compare("jgt") == 0)) {
            kodiran = Konvert::hexToBinary5(Instrukcije::vratiHex(inst));
            if (uradi){
				kodiran += "000";
            }else {
				kodiran += "100";
			}
			t.kodovanInstr.push_back(Konvert::binaryToHex8(kodiran) + " ");
            if ((op1.find('*') != string::npos)) {
                if (Konvert::proveraHexVrednosti(op1.erase(0, 1))) {
					pomocnaFunkcijaAdresiranje1(op1, t, "10100000");
                    continue;
                }
            }
            if ((op1.find('$') != string::npos)) {
                if (TabelaSimbola::pronadji(op1.erase(0, 1))) {
				if ((inst.compare("jne") == 0) || (inst.compare("jeq") == 0) || (inst.compare("call") == 0)
						|| (inst.compare("jmp") == 0)|| (inst.compare("jgt") == 0)) {
						t.kodovanInstr.push_back(Konvert::binaryToHex8("10110000") + " ");
						if (TabelaSimbola::checkLokal(op1)) {
							TabelaRelokacije::dodaj(t.ime, "R_386_PC16",
													TabelaSimbola::vratiBR(op1),
													Konvert::konvertujUnIntToHex(t.brojac+2));
							string s = Konvert::vratiHexNa4B(Konvert::konvertujUnIntToHex(
								TabelaSimbola::getOfset(op1)-2-t.brojac));
							t.kodovanInstr.push_back(s.substr(0, 2) + " ");
							t.kodovanInstr.push_back(s.substr(2, 2) + " ");
						} else {
							TabelaRelokacije::dodaj(t.ime, "R_386_PC16",
													TabelaSimbola::vratiBR(op1),
													Konvert::konvertujUnIntToHex(t.brojac + 2));
							t.kodovanInstr.push_back("FF ");
							t.kodovanInstr.push_back("FE ");
						}
					} else {
						t.kodovanInstr.push_back(Konvert::binaryToHex8("10100000") + " ");
						if (TabelaSimbola::checkLokal(op1)) {
							TabelaRelokacije::dodaj(t.ime, "R_386_16",
													TabelaSimbola::vratiBR(op1),
													Konvert::konvertujUnIntToHex(t.brojac + 2));
							string s = Konvert::vratiHexNa4B(Konvert::
															 konvertujUnIntToHex(TabelaSimbola::getOfset(op1)));
							t.kodovanInstr.push_back(s.substr(0, 2) + " ");
							t.kodovanInstr.push_back(s.substr(2, 2) + " ");
						} else {
							TabelaRelokacije::dodaj(t.ime, "R_386_16",
													TabelaSimbola::vratiBR(op1),
													Konvert::konvertujUnIntToHex(t.brojac + 2));
							t.kodovanInstr.push_back("00 ");
							t.kodovanInstr.push_back("00 ");
						}
					}
					t.brojac += 4;
					continue;
                } else {
                    cout << "nema simbola " << t.tokeniSekcije[q] << endl;
                    throw 1;
                }
            }
            if (Registri::pronadji(op1.substr(0,2))) {
				if(op1.length()==2){
					t.kodovanInstr.push_back(Konvert::binaryToHex8("0010" +
						Registri::vratiHex(op1) + "0") + " ");
					t.brojac += 2;
					continue;
				}
				if(op1.substr(2,1).compare("[")!=0){
					if(op1.length()==3){
						if(op1.substr(2,1).compare("l")==0){
							t.kodovanInstr.push_back(Konvert::binaryToHex8("0010" +
								Registri::vratiHex(op1.substr(0,2)) + "0") + " ");
							t.brojac += 2;
							continue;
						}
						if(op1.substr(2,1).compare("h")==0){
							t.kodovanInstr.push_back(Konvert::binaryToHex8("0010" +
								Registri::vratiHex(op1.substr(0,2)) + "1") + " ");
							t.brojac += 2;
							continue;
						}
					}
				}
            }
            if (TabelaSimbola::pronadji(op1)) {
                if ((inst.compare("jne") == 0) || (inst.compare("jeq") == 0) || (inst.compare("call") == 0)
                    || (inst.compare("jmp") == 0)|| (inst.compare("jgt") == 0)) {
                    t.kodovanInstr.push_back(Konvert::binaryToHex8("10110000") + " ");
                    if (TabelaSimbola::checkLokal(op1)) {
                        TabelaRelokacije::dodaj(t.ime, "R_386_PC16",
                                                TabelaSimbola::vratiBR(op1),
                                                Konvert::konvertujUnIntToHex(t.brojac+2));
                        string s = Konvert::vratiHexNa4B(Konvert::konvertujUnIntToHex(
							TabelaSimbola::getOfset(op1)-2-t.brojac));
                        t.kodovanInstr.push_back(s.substr(0, 2) + " ");
                        t.kodovanInstr.push_back(s.substr(2, 2) + " ");
                    } else {
                        TabelaRelokacije::dodaj(t.ime, "R_386_PC16",
                                                TabelaSimbola::vratiBR(op1),
                                                Konvert::konvertujUnIntToHex(t.brojac + 2));
                        t.kodovanInstr.push_back("FF ");
                        t.kodovanInstr.push_back("FE ");
                    }
                } else {
                    t.kodovanInstr.push_back(Konvert::binaryToHex8("10100000") + " ");
                    if (TabelaSimbola::checkLokal(op1)) {
                        TabelaRelokacije::dodaj(t.ime, "R_386_16",
                                                TabelaSimbola::vratiBR(op1),
                                                Konvert::konvertujUnIntToHex(t.brojac + 2));
                        string s = Konvert::vratiHexNa4B(Konvert::
                                                         konvertujUnIntToHex(TabelaSimbola::getOfset(op1)));
                        t.kodovanInstr.push_back(s.substr(0, 2) + " ");
                        t.kodovanInstr.push_back(s.substr(2, 2) + " ");
                    } else {
                        TabelaRelokacije::dodaj(t.ime, "R_386_16",
                                                TabelaSimbola::vratiBR(op1),
                                                Konvert::konvertujUnIntToHex(t.brojac + 2));
                        t.kodovanInstr.push_back("00 ");
                        t.kodovanInstr.push_back("00 ");
                    }
                }
                t.brojac += 4;
                continue;
            }
            if (Registri::pronadji(op1.substr(0, 2))) {
				string reg=op1.substr(0,2);
                op1.erase(0, 2);
                if ((op1.substr(0, 1).compare("[") == 0) && (op1.substr(op1.length() - 1, 1).compare("]") == 0)) {
                    if (TabelaSimbola::pronadji(op1.substr(1, op1.length() - 2))) {
                        t.kodovanInstr.push_back(Konvert::binaryToHex8("1000"+Registri::vratiHex(reg)+"0") + " ");
                        doSmth(t, op1.substr(1, op1.length() - 2), true);
                        t.brojac += 4;
                        continue;
                    }
                    if (Konvert::proveraHexVrednosti(op1.substr(1, op1.length() - 2))) {
						if(Konvert::vratiHexNa4B(op1.substr(1, op1.length() - 2)).compare("0000")==0){
							t.kodovanInstr.push_back(Konvert::binaryToHex8("0100"+Registri::vratiHex(reg)+"0")+" ");
							t.brojac+=2;
							continue;
						}
						string w=op1.substr(1, op1.length() - 2);
						if((Konvert::konvertujStringToInt(w)<=127)&&(Konvert::konvertujStringToInt(w)>=-128)){
							t.kodovanInstr.push_back(Konvert::binaryToHex8("0110"+Registri::vratiHex(reg)+"0") + " ");
							string s = Konvert::vratiHexNa4B(w);
							t.kodovanInstr.push_back(s.substr(2, 2) + " ");
							t.brojac += 2;
							continue;
						}
						pomocnaFunkcijaAdresiranje1(w, t, "1000"+Registri::vratiHex(reg)+"0");
						continue;
                    }
                }
            }
            cout << "greska adresiranja " << t.tokeniSekcije[q] << endl;
            throw 1;
        }

        if ((inst.compare("push") == 0)) {
            kodiran = Konvert::hexToBinary5(Instrukcije::vratiHex(inst));
            if (uradi){
				kodiran += "000";
            }else {
				kodiran += "100";
			}
            t.kodovanInstr.push_back(Konvert::binaryToHex8(kodiran) + " ");
            if (Konvert::proveraHexVrednosti(op1)) {
				pomocnaFunkcijaAdresiranje1(op1, t, "00000000");
				continue;
            }
            if (op1.find('&') != string::npos) {
                if (TabelaSimbola::pronadji(op1.erase(0, 1))) {
					if(TabelaSimbola::checkABS(op1)){
						string h=Konvert::konvertujIntToHex(TabelaSimbola::getOfset(op1));
						pomocnaFunkcijaAdresiranje1(h ,t , "00000000");
						continue;
					}
                    t.kodovanInstr.push_back(Konvert::binaryToHex8("10100000") + " ");
                    doSmth(t, op1, false);
                    t.brojac += 4;
                    continue;
                } else {
                    cout << "nema simbola " << t.tokeniSekcije[q] << endl;
                    throw 1;
                }
            }
            if ((op1.find('*') != string::npos))
                if (Konvert::proveraHexVrednosti(op1.erase(0, 1))) {
					pomocnaFunkcijaAdresiranje1(op1, t, "10100000");
                    continue;
                }
            if ((op1.find('$') != string::npos)) {
                if (TabelaSimbola::pronadji(op1.erase(0, 1))) {
                   t.kodovanInstr.push_back(Konvert::binaryToHex8("10110000") + " ");
					if (TabelaSimbola::checkLokal(op1)) {
						TabelaRelokacije::dodaj(t.ime, "R_386_PC16",
												TabelaSimbola::vratiBR(op1),
												Konvert::konvertujUnIntToHex(t.brojac+2));
						string s = Konvert::vratiHexNa4B(Konvert::konvertujUnIntToHex(
							TabelaSimbola::getOfset(op1)-2-t.brojac));
						t.kodovanInstr.push_back(s.substr(0, 2) + " ");
						t.kodovanInstr.push_back(s.substr(2, 2) + " ");
					} else {
						TabelaRelokacije::dodaj(t.ime, "R_386_PC16",
												TabelaSimbola::vratiBR(op1),
												Konvert::konvertujUnIntToHex(t.brojac + 2));
						t.kodovanInstr.push_back("FF ");
						t.kodovanInstr.push_back("FE ");
					}
					t.brojac += 4;
					continue;
                } else {
                    cout << "nema simbola " << t.tokeniSekcije[q] << endl;
                    throw 1;
                }
            }
            if (Registri::pronadji(op1.substr(0,2))) {
				if(op1.length()==2){
					t.kodovanInstr.push_back(Konvert::binaryToHex8("0010" +
						Registri::vratiHex(op1) + "0") + " ");
					t.brojac += 2;
					continue;
				}
				if(op1.substr(2,1).compare("[")!=0){
					if(op1.length()==3){
						if(op1.substr(2,1).compare("l")==0){
							t.kodovanInstr.push_back(Konvert::binaryToHex8("0010" +
								Registri::vratiHex(op1.substr(0,2)) + "0") + " ");
							t.brojac += 2;
							continue;
						}
						if(op1.substr(2,1).compare("h")==0){
							t.kodovanInstr.push_back(Konvert::binaryToHex8("0010" +
								Registri::vratiHex(op1.substr(0,2)) + "1") + " ");
							t.brojac += 2;
							continue;
						}
					}
				}
            }
            if (TabelaSimbola::pronadji(op1)) {
                t.kodovanInstr.push_back(Konvert::binaryToHex8("10100000") + " ");
				doSmth(t, op1, false);
                t.brojac += 4;
                continue;
            }
            if (Registri::pronadji(op1.substr(0, 2))) {
				string reg=op1.substr(0,2);
                op1.erase(0, 2);
                if ((op1.substr(0, 1).compare("[") == 0) && (op1.substr(op1.length() - 1, 1).compare("]") == 0)) {
                    if (TabelaSimbola::pronadji(op1.substr(1, op1.length() - 2))) {
                        t.kodovanInstr.push_back(Konvert::binaryToHex8("1000"+Registri::vratiHex(reg)+"0") + " ");
                        doSmth(t, op1.substr(1, op1.length() - 2), true);
                        t.brojac += 4;
                        continue;
                    }
                    if (Konvert::proveraHexVrednosti(op1.substr(1, op1.length() - 2))) {
						if(Konvert::vratiHexNa4B(op1.substr(1, op1.length() - 2)).compare("0000")==0){
							t.kodovanInstr.push_back(Konvert::binaryToHex8("0100"+Registri::vratiHex(reg)+"0")+" ");
							t.brojac+=2;
							continue;
						}
						string w=op1.substr(1, op1.length() - 2);
						if((Konvert::konvertujStringToInt(w)<=127)&&(Konvert::konvertujStringToInt(w)>=-128)){
							t.kodovanInstr.push_back(Konvert::binaryToHex8("0110"+Registri::vratiHex(reg)+"0") + " ");
							string s = Konvert::vratiHexNa4B(w);
							t.kodovanInstr.push_back(s.substr(2, 2) + " ");
							t.brojac += 2;
							continue;
						}
						pomocnaFunkcijaAdresiranje1(w, t, "1000"+Registri::vratiHex(reg)+"0");
						continue;
                    }
                }
            }
            cout << "greska adresiranja " << t.tokeniSekcije[q] << endl;
            throw 1;
        }
        if ((inst.compare("xchg") == 0) || (inst.compare("mov") == 0) || (inst.compare("add") == 0)
            || (inst.compare("sub") == 0) || (inst.compare("mul") == 0) || (inst.compare("div") == 0)
            || (inst.compare("cmp") == 0) || (inst.compare("and") == 0) || (inst.compare("or") == 0) ||
            (inst.compare("div") == 0)|| (inst.compare("xor") == 0) || (inst.compare("test") == 0) 
            || (inst.compare("shl") == 0) || (inst.compare("div") == 0) || (inst.compare("shr") == 0)) {
            kodiran = Konvert::hexToBinary5(Instrukcije::vratiHex(inst));
            if (uradi){
				kodiran += "000";
            }else {
				kodiran += "100";
			}
            t.kodovanInstr.push_back(Konvert::binaryToHex8(kodiran) + " ");
            if ((op1.find('*') != string::npos)) {
                if (Konvert::proveraHexVrednosti(op1.erase(0, 1))) {
                    pomocnaFunkcijaAdresiranje1(op1, t, "10100000");
                }
            }
            if ((op1.find('$') != string::npos)) {
                if (TabelaSimbola::pronadji(op1.erase(0, 1))) {
					t.kodovanInstr.push_back(Konvert::binaryToHex8("10110000") + " ");
					if (TabelaSimbola::checkLokal(op1)) {
						TabelaRelokacije::dodaj(t.ime, "R_386_PC16",
												TabelaSimbola::vratiBR(op1),
												Konvert::konvertujUnIntToHex(t.brojac+2));
						string s = Konvert::vratiHexNa4B(Konvert::konvertujUnIntToHex(
							TabelaSimbola::getOfset(op1)-2-t.brojac));
						t.kodovanInstr.push_back(s.substr(0, 2) + " ");
						t.kodovanInstr.push_back(s.substr(2, 2) + " ");
					} else {
						TabelaRelokacije::dodaj(t.ime, "R_386_PC16",
												TabelaSimbola::vratiBR(op1),
												Konvert::konvertujUnIntToHex(t.brojac + 2));
						t.kodovanInstr.push_back("FF ");
						t.kodovanInstr.push_back("FE ");
					}
					t.brojac += 4;
                } else {
                    cout << "nema simbola " << t.tokeniSekcije[q] << endl;
                    throw 1;
                }
            }
            if (Registri::pronadji(op1.substr(0,2))) {
				if(op1.length()==2){
					t.kodovanInstr.push_back(Konvert::binaryToHex8("0010" +
						Registri::vratiHex(op1) + "0") + " ");
					t.brojac += 2;
				}
				if(op1.substr(2,1).compare("[")!=0){
					if(op1.length()==3){
						if(op1.substr(2,1).compare("l")==0){
							t.kodovanInstr.push_back(Konvert::binaryToHex8("0010" +
								Registri::vratiHex(op1.substr(0,2)) + "0") + " ");
							t.brojac += 2;
						}
						if(op1.substr(2,1).compare("h")==0){
							t.kodovanInstr.push_back(Konvert::binaryToHex8("0010" +
								Registri::vratiHex(op1.substr(0,2)) + "1") + " ");
							t.brojac += 2;
						}
					}
				}
            }
            if (TabelaSimbola::pronadji(op1)) {
                t.kodovanInstr.push_back(Konvert::binaryToHex8("10100000") + " ");
				doSmth(t, op1, false);
                t.brojac += 4;
            }
            if (Registri::pronadji(op1.substr(0, 2))) {
				string reg=op1.substr(0,2);
                op1.erase(0, 2);
                if ((op1.substr(0, 1).compare("[") == 0) && (op1.substr(op1.length() - 1, 1).compare("]") == 0)) {
                    if (TabelaSimbola::pronadji(op1.substr(1, op1.length() - 2))) {
                        t.kodovanInstr.push_back(Konvert::binaryToHex8("1000"+Registri::vratiHex(reg)+"0") + " ");
                        doSmth(t, op1.substr(1, op1.length() - 2), true);
                        t.brojac += 4;
                    }
                    string w=op1.substr(1, op1.length() - 2);
                    if (Konvert::proveraHexVrednosti(op1.substr(1, op1.length() - 2))) {
						if(Konvert::vratiHexNa4B(op1.substr(1, op1.length() - 2)).compare("0000")==0){
							t.kodovanInstr.push_back(Konvert::binaryToHex8("0100"+Registri::vratiHex(reg)+"0")+" ");
							t.brojac+=2;
						}else if((Konvert::konvertujStringToInt(w)<=127)&&(Konvert::konvertujStringToInt(w)>=-128)){
							t.kodovanInstr.push_back(Konvert::binaryToHex8("0110"+Registri::vratiHex(reg)+"0") + " ");
							string s = Konvert::vratiHexNa4B(w);
							t.kodovanInstr.push_back(s.substr(2, 2) + " ");
							t.brojac += 3;
						}else{
							pomocnaFunkcijaAdresiranje1(op1.substr(1, op1.length() - 2), t, "1000"+Registri::vratiHex(reg)+"0");
						}
					}
                }
            }
            string op2 = tokenLinija[2];
            t.brojac--;
            if (Konvert::proveraHexVrednosti(op2)) {
                pomocnaFunkcijaAdresiranje1(op2, t, "00000000");
				continue;
            }
            if (op2.find('&') != string::npos) {
                if (TabelaSimbola::pronadji(op2.erase(0, 1))) {
					if(TabelaSimbola::checkABS(op2)){
						string h=Konvert::konvertujIntToHex(TabelaSimbola::getOfset(op2));
						pomocnaFunkcijaAdresiranje1(h ,t , "00000000");
						continue;
					}
					t.kodovanInstr.push_back(Konvert::binaryToHex8("10100000") + " ");
                    doSmth(t, op2, false);
                    t.brojac += 4;
                    continue;
                } else {
                    cout << "nema simbola " << t.tokeniSekcije[q] << endl;
                    throw 1;
                }
            }
            if ((op2.find('*') != string::npos))
                if (Konvert::proveraHexVrednosti(op2.erase(0, 1))) {
                    pomocnaFunkcijaAdresiranje1(op2, t, "10100000");
                    continue;
                }
            if ((op2.find('$') != string::npos)) {
                if (TabelaSimbola::pronadji(op2.erase(0, 1))) {
                    t.kodovanInstr.push_back(Konvert::binaryToHex8("10110000") + " ");
					if (TabelaSimbola::checkLokal(op2)) {
						TabelaRelokacije::dodaj(t.ime, "R_386_PC16",
												TabelaSimbola::vratiBR(op2),
												Konvert::konvertujUnIntToHex(t.brojac+2));
						string s = Konvert::vratiHexNa4B(Konvert::konvertujUnIntToHex(
							TabelaSimbola::getOfset(op2)-2-t.brojac));
						t.kodovanInstr.push_back(s.substr(0, 2) + " ");
						t.kodovanInstr.push_back(s.substr(2, 2) + " ");
					} else {
						TabelaRelokacije::dodaj(t.ime, "R_386_PC16",
												TabelaSimbola::vratiBR(op2),
												Konvert::konvertujUnIntToHex(t.brojac + 2));
						t.kodovanInstr.push_back("FF ");
						t.kodovanInstr.push_back("FE ");
					}
					t.brojac += 4;
					continue;
                } else {
                    cout << "nema simbola " << t.tokeniSekcije[q] << endl;
                    throw 1;
                }
            }
            if (Registri::pronadji(op2.substr(0,2))) {
				if(op2.length()==2){
					t.kodovanInstr.push_back(Konvert::binaryToHex8("0010" +
						Registri::vratiHex(op2) + "0") + " ");
					t.brojac += 2;
					continue;
				}
				if(op2.substr(2,1).compare("[")!=0){
					if(op2.length()==3){
						if(op2.substr(2,1).compare("l")==0){
							t.kodovanInstr.push_back(Konvert::binaryToHex8("0010" +
								Registri::vratiHex(op2.substr(0,2)) + "0") + " ");
							t.brojac += 2;
							continue;
						}
						if(op2.substr(2,1).compare("h")==0){
							t.kodovanInstr.push_back(Konvert::binaryToHex8("0010" +
								Registri::vratiHex(op2.substr(0,2)) + "1") + " ");
							t.brojac += 2;
							continue;
						}
					}
				}
            }
            if (TabelaSimbola::pronadji(op2)) {
                t.kodovanInstr.push_back(Konvert::binaryToHex8("10100000") + " ");     
                doSmth(t, op2, false);
                t.brojac += 4;
                continue;
            }
            if (Registri::pronadji(op2.substr(0, 2))) {
				string reg=op2.substr(0,2);
                op2.erase(0, 2);
                if ((op2.substr(0, 1).compare("[") == 0) && (op2.substr(op2.length() - 1, 1).compare("]") == 0)) {
                    if (TabelaSimbola::pronadji(op2.substr(1, op2.length() - 2))) {
                        t.kodovanInstr.push_back(Konvert::binaryToHex8("1000"+Registri::vratiHex(reg)+"0") + " ");
                        doSmth(t, op2.substr(1, op2.length() - 2), true);
                        t.brojac += 4;
                        continue;
                    }
                    if (Konvert::proveraHexVrednosti(op2.substr(1, op2.length() - 2))) {
						if(Konvert::vratiHexNa4B(op2.substr(1, op2.length() - 2)).compare("0000")==0){
							t.kodovanInstr.push_back(Konvert::binaryToHex8("0100"+Registri::vratiHex(reg)+"0")+" ");
							t.brojac+=2;
							continue;
						}
						string w=op2.substr(1, op2.length() - 2);
						if((Konvert::konvertujStringToInt(w)<=127)&&(Konvert::konvertujStringToInt(w)>=-128)){
							t.kodovanInstr.push_back(Konvert::binaryToHex8("0110"+Registri::vratiHex(reg)+"0") + " ");
							string s = Konvert::vratiHexNa4B(w);
							t.kodovanInstr.push_back(s.substr(2, 2) + " ");
							t.brojac += 3;
							continue;
						}
						pomocnaFunkcijaAdresiranje1(w, t, "1000"+Registri::vratiHex(reg)+"0");
						continue;
                    }
                }
            }
            cout << "greska adresiranja " << t.tokeniSekcije[q] << endl;
            throw 1;
        }
        string s = tokenLinija[1];
        if (inst.compare(".byte") == 0) {
            if (Konvert::proveraHexVrednosti(s)) {
                s.erase(0, 2);
                t.brojac++;
                if (s.length() == 1) {
                    t.kodovanInstr.push_back("0" + s + " ");
                    continue;
                }
                if (s.length() == 2) {
                    t.kodovanInstr.push_back(s + " ");
                    continue;
                }
                cout << "hex vrednost je pogresne duzine " << t.tokeniSekcije[q] << endl;
                throw 1;
            }  else if (TabelaSimbola::pronadji(s)) {
                izlazPRW x;
                if (TabelaSimbola::checkLokal(s)) {
                    TabelaRelokacije::dodaj(t.ime, "R_386_16",
                                            TabelaSimbola::vratiBR(s),
                                            Konvert::konvertujUnIntToHex(t.brojac));
                    string b = Konvert::vratiHexNa2B(Konvert::
                                                     konvertujUnIntToHex(TabelaSimbola::getOfset(s)));
                    x.ofset = t.brojac;
                    x.sadrzajHex = b.substr(0, 2);
                    t.nizPodataka.push_back(x);
                    t.brojac++;
                } else {
                    TabelaRelokacije::dodaj(t.ime, "R_386_16",
                                            TabelaSimbola::vratiBR(s),
                                            Konvert::konvertujUnIntToHex(t.brojac));
                    x.ofset = t.brojac;
                    x.sadrzajHex = "00";
                    t.nizPodataka.push_back(x);
                    t.brojac++;
                }
            } else {
                cout << "parametri .byte direktive nisu validni " << t.tokeniSekcije[q] << endl;
                throw 1;
            }
            continue;
        }
        if (inst.compare(".word") == 0) {
            if (Konvert::proveraHexVrednosti(s)) {
                s.erase(0, 2);
                t.brojac++;
                if (s.length() == 1) {
                    t.kodovanInstr.push_back("0" + s + " ");
                    continue;
                }
                if (s.length() == 2) {
                    t.kodovanInstr.push_back(s + " ");
                    continue;
                }
                t.brojac++;
                if (s.length() == 3) {
                    t.kodovanInstr.push_back("0" + s.substr(0, 1) + " ");
                    t.kodovanInstr.push_back(s.substr(1, 2) + " ");
                    continue;
                }
                if (s.length() == 4) {
                    t.kodovanInstr.push_back(s.substr(0, 2) + " ");
                    t.kodovanInstr.push_back(s.substr(2, 2) + " ");
                    continue;
                }
            } else if (TabelaSimbola::pronadji(s)) {
                t.brojac += 2;
                if (TabelaSimbola::checkLokal(s)) {
                    TabelaRelokacije::dodaj(t.ime, "R_386_16",
                                            TabelaSimbola::vratiBR(s),
                                            Konvert::konvertujUnIntToHex(t.brojac));
                    string b = Konvert::vratiHexNa4B(Konvert::
                                                     konvertujUnIntToHex(TabelaSimbola::getOfset(s)));
                    t.kodovanInstr.push_back(b.substr(0, 2) + " ");
                    t.kodovanInstr.push_back(b.substr(2, 2) + " ");
                } else {
                    TabelaRelokacije::dodaj(t.ime, "R_386_16",
                                            TabelaSimbola::vratiBR(s),
                                            Konvert::konvertujUnIntToHex(t.brojac));
                    t.kodovanInstr.push_back("00 ");
                    t.kodovanInstr.push_back("00 ");
                }
            } else {
                cout << "parametri .word direktive nisu validni " << t.tokeniSekcije[q] << endl;
                throw 1;
            }
            continue;
        }
        if (inst.compare(".align") == 0) {
            if (Konvert::proveraHexVrednosti(s)) {
                unsigned int p = Konvert::konvertujStringToUnInt(s);
                for (unsigned int q = 0; q < p; q ++) {
                    t.kodovanInstr.push_back("FF ");//NOP instrukcija
                    t.brojac++;
                }
            } else {
                cout << "hex vrednost nije validna " << t.tokeniSekcije[q] << endl;
                throw 1;
            }
            continue;
        }
        if (inst.compare(".skip") == 0) {
            if (Konvert::proveraHexVrednosti(s)) {
                unsigned int p = Konvert::konvertujStringToUnInt(s);
                for (unsigned int q = 0; q < p; q ++) {
                    t.kodovanInstr.push_back("00 ");
                    t.brojac++;
                }
            } else {
                cout << "hex vrednost nije validna " << t.tokeniSekcije[q] << endl;
                throw 1;
            }
            continue;
        }
    }
}

void TabelaSekcija::pomocnaFunkcijaAdresiranje1(string op, TabelaSekcija &t, string x){
	t.kodovanInstr.push_back(Konvert::binaryToHex8(x) + " ");
	string s = Konvert::vratiHexNa4B(op);
	t.kodovanInstr.push_back(s.substr(0, 2) + " ");
	t.kodovanInstr.push_back(s.substr(2, 2) + " ");
	t.brojac += 4;
}

void TabelaSekcija::doSmth(TabelaSekcija &t, string op1, bool bitPomer8) {
    if (TabelaSimbola::checkLokal(op1)) {
        TabelaRelokacije::dodaj(t.ime, "R_386_16",
			TabelaSimbola::vratiBR(op1), Konvert::konvertujUnIntToHex(t.brojac + 2));
        string s = ""; 
        if (bitPomer8){
			s=Konvert::vratiHexNa4BInt(Konvert::
				konvertujIntToHex(TabelaSimbola::getOfset(op1)));
        }else {
			s=Konvert::vratiHexNa4B(Konvert::
				konvertujUnIntToHex(TabelaSimbola::getOfset(op1)));
		}
        t.kodovanInstr.push_back(s.substr(0, 2) + " ");
        t.kodovanInstr.push_back(s.substr(2, 2) + " ");
    } else {
        TabelaRelokacije::dodaj(t.ime, "R_386_16",
			TabelaSimbola::vratiBR(op1), Konvert::konvertujUnIntToHex(t.brojac + 2));
        t.kodovanInstr.push_back("00 ");
        t.kodovanInstr.push_back("00 ");
    }
}

void TabelaSekcija::izlaz() {
	Prolazi::izlaz << "#PocinjeSVE"<<endl;
	Prolazi::izlaz << "TabelaSekcija";
    for (unsigned int i = 0; i < nizTabelaSekcija.size(); i++) {
		if(nizTabelaSekcija[i].velicina!=0){
			if (nizTabelaSekcija[i].flegovi.compare("PRW") == 0) {
				Prolazi::izlaz << endl << nizTabelaSekcija[i].ime <<" PRW"<< endl;
				for (unsigned int j = 0; j < nizTabelaSekcija[i].nizPodataka.size(); j++)
					Prolazi::izlaz << nizTabelaSekcija[i].nizPodataka[j].sadrzajHex << " ";
			}
			if (nizTabelaSekcija[i].flegovi.compare("PXR") == 0) {
				Prolazi::izlaz << endl << nizTabelaSekcija[i].ime<<" PXR" << endl;
				for (unsigned int j = 0; j < nizTabelaSekcija[i].kodovanInstr.size(); j++)
					Prolazi::izlaz << nizTabelaSekcija[i].kodovanInstr[j]<< " ";
			}
			if (nizTabelaSekcija[i].flegovi.compare("RW") == 0){
				Prolazi::izlaz << endl << nizTabelaSekcija[i].ime<<" RW" << endl;
				Prolazi::izlaz << nizTabelaSekcija[i].velicina << " ";
			}
		}
    }
    Prolazi::izlaz << endl;
}
