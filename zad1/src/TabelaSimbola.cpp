#include "TabelaSimbola.h"
#include "Prolazi.h"
#include <fstream>

vector<TabelaSimbola> TabelaSimbola::nizSimbola = init();
unsigned int TabelaSimbola::REDNIBROJ = 0;

TabelaSimbola::TabelaSimbola(string l, string s, string lo, unsigned int o) {
    labela = l;
    sekcija = s;
    lokal = lo;
    ofset = o;
    redniBr = REDNIBROJ;
    REDNIBROJ++;
    nizSimbola.push_back(TabelaSimbola(this));
}

TabelaSimbola::TabelaSimbola(TabelaSimbola *t) {
    labela = t->labela;
    sekcija = t->sekcija;
    lokal = t->lokal;
    ofset = t->ofset;
    redniBr = t->redniBr;
}

void TabelaSimbola::setABS(string s){
	for (unsigned int i = 0; i < nizSimbola.size(); i++)
        if (s.compare(nizSimbola[i].labela) == 0){
			nizSimbola[i].abs=1;
			break;
		}
}

bool TabelaSimbola::checkABS(string s){
	for (unsigned int i = 0; i < nizSimbola.size(); i++)
        if (s.compare(nizSimbola[i].labela) == 0){
			if(nizSimbola[i].abs==1)
				return true;
			return false;
		}
	return false;
}

unsigned int TabelaSimbola::vratiBR(string s) {
    for (unsigned int i = 0; i < nizSimbola.size(); i++)
        if (s.compare(nizSimbola[i].labela) == 0) {
            if (nizSimbola[i].lokal.compare("L") == 0) {
                for (unsigned int j = 0; j < nizSimbola.size(); j++)
                    if (nizSimbola[i].sekcija.compare(nizSimbola[j].labela) == 0)
                        return nizSimbola[j].redniBr;
            } else {
                return nizSimbola[i].redniBr;
            }
        }
    cout << "greska sistema simbol ne postoji" << endl;
    throw 1;
}

bool TabelaSimbola::checkLokal(string s) {
    for (unsigned int i = 0; i < nizSimbola.size(); i++)
        if (s.compare(nizSimbola[i].labela) == 0) {
            if (nizSimbola[i].lokal.compare("L") == 0) {
                return true;
            } else {
                return false;
            }
        }
    cout << "greska sistema simbol ne postoji" << endl;
    throw 1;
}

string TabelaSimbola::getSekcija(string s) {
    for (unsigned int i = 0; i < nizSimbola.size(); i++)
        if (s.compare(nizSimbola[i].labela) == 0) {
            return nizSimbola[i].sekcija;
        }
    cout << "greska sistema simbol ne postoji" << endl;
    throw 1;
}

string TabelaSimbola::getLokal(string s) {
    for (unsigned int i = 0; i < nizSimbola.size(); i++)
        if (s.compare(nizSimbola[i].labela) == 0) {
            return nizSimbola[i].lokal;
        }
    cout << "greska sistema simbol ne postoji" << endl;
    throw 1;
}

unsigned int TabelaSimbola::getOfset(string s) {
    for (unsigned int i = 0; i < nizSimbola.size(); i++)
        if (s.compare(nizSimbola[i].labela) == 0) {
            return nizSimbola[i].ofset;
        }
    cout << "greska sistema simbol ne postoji" << endl;
    throw 1;
}

void TabelaSimbola::setLokal(string s) {
    lokal = s;
}

void TabelaSimbola::setOfset(unsigned int o) {
    ofset = o;
}

void TabelaSimbola::setSekcija(string s) {
    sekcija = s;
}

bool TabelaSimbola::pronadji(string s) {
    for (unsigned int i = 0; i < nizSimbola.size(); i++)
        if (s.compare(nizSimbola[i].labela) == 0)
            return true;
    return false;
}

vector<TabelaSimbola> TabelaSimbola::init() {
    TabelaSimbola tab = TabelaSimbola("*UNDEFINED*:", "-2", "L", 0);
    vector<TabelaSimbola> t;
    t.push_back(tab);
    return t;
}

bool TabelaSimbola::canADD(string s, string t, unsigned int b) {
    for (unsigned int i = 0; i < nizSimbola.size(); i++)
        if (s.compare(nizSimbola[i].labela) == 0) {
            if (nizSimbola[i].sekcija.compare("-1") == 0) {
                nizSimbola[i].setSekcija(t);
                nizSimbola[i].setOfset(b);
                return true;
            } else
                return false;
        }
    return false;
}

void TabelaSimbola::ispis() {
	Prolazi::izlaz << "TabelaSimbola" << endl;
    for (unsigned int i = 0; i < nizSimbola.size(); i++) {
        Prolazi::izlaz << nizSimbola[i].labela << "	" << nizSimbola[i].sekcija << "	" <<
			nizSimbola[i].lokal << "	" << nizSimbola[i].ofset << "	" << nizSimbola[i].redniBr << endl;
    }
}
