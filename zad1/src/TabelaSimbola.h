#ifndef __TabelaSimbola_H_
#define __TabelaSimbola_H_

#include <iostream>
#include <cstring>
#include <vector>

using namespace std;

class TabelaSimbola {
	private:
		string labela, sekcija, lokal;
		unsigned int ofset, redniBr, abs;
		static unsigned int REDNIBROJ;
		static vector<TabelaSimbola> nizSimbola;
		static vector<TabelaSimbola> init();
		TabelaSimbola(TabelaSimbola *t);

	public:
		TabelaSimbola(string l, string s, string lo, unsigned int o);
		void setLokal(string s);
		static void setABS(string s);
		static bool checkABS(string s);
		void setOfset(unsigned int o);
		void setSekcija(string s);
		static bool pronadji(string s);
		static unsigned int getOfset(string s);
		static string getSekcija(string s);
		static string getLokal(string s);
		static bool canADD(string s, string t, unsigned int b);
		static void ispis();
		static unsigned int vratiBR(string s);
		static bool checkLokal(string s);
};

#endif

