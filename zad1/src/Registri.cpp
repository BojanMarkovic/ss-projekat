#include "Registri.h"


string Registri::nizRegistara[] = {"r0", "r1", "r2", "r3", "r4", "r5", "r6", "r7", "sp", "pc"};

string Registri::nizSifara[] = {"000", "001", "010", "011", "100", "101",
                                "110", "111", "110", "111"};

bool Registri::pronadji(string s) {
    for (int i = 0; i < 10; i++)
        if (s.compare(nizRegistara[i]) == 0)
            return true;
    return false;
}

string Registri::vratiHex(string s) {
    for (int i = 0; i < 10; i++)
        if (s.compare(nizRegistara[i]) == 0)
            return nizSifara[i];
    return NULL;
}
