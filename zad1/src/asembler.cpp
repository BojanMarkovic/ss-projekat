#include <iostream>
#include <cstring>
#include "Prolazi.h"
#include "Instrukcije.h"

using namespace std;

int main(int argc, char *argv[]) {
    try {
        string in = argv[3];
        char *ulazFajl = new char[in.length() + 1];
        strcpy(ulazFajl, in.c_str());
        in = argv[2];
        char *izlazniFajl = new char[in.length() + 1];
        strcpy(izlazniFajl, in.c_str());
        Prolazi::pass1(ulazFajl);
        //cout << "Zavrsen prvi prolaz!" << endl;
        Prolazi::pass2(izlazniFajl);
        cout << "Zavrsen prolaz asemblera!" << endl;
        return 0;
    } catch (int e) {
        cout << "Dogodila se greska broj" << e + 1 << endl;
    } catch (...) {
        cout << "Dogodila se fatalna greska!" << endl;
    }

}
