#include "Konvert.h"

string Konvert::hexCifre="0123456789ABCDEF";

unsigned int Konvert::konvertujStringToUnInt(string &s){
	unsigned int x;   
    std::stringstream ss;
    ss << std::hex << s;
    ss >> x;
    return x;
}

int Konvert::konvertujStringToInt(string &s){
	int x;   
    std::stringstream ss;
    ss << std::hex << s;
    ss >> x;
    if(x>32767)
		x-=65536;
    return x;
}

string Konvert::hexToBinary5(string s){
	unsigned int i=konvertujStringToUnInt(s);
	bitset<5> b(i);
	return b.to_string();
}

string Konvert::vratiHexNa4BInt(string s){
	int i =konvertujStringToInt(s);
	s = konvertujUnIntToHex(konvertujStringToUnInt(s));
	if(s.length()>4)
		s=s.substr(s.length()-4,4);
	if(s.length()==1){
		return "000"+s;
	}
	if(s.length()==2){
		if(i<0){
			return "FF"+s;
		}else
			return "00"+s;
	}
	if(s.length()==3){
		return "0"+s;
	}
	if(s.length()==4){
		return s;
	}
	cout<<"konverzija nije validne duzine parametar je predugacak"<<endl;
	throw 1;
}

string Konvert::vratiHexNa4B(string s){
	s = konvertujUnIntToHex(konvertujStringToUnInt(s));
	if(s.length()>4)
		s=s.substr(s.length()-4,4);
	if(s.length()==1){
		return "000"+s;
	}
	if(s.length()==2){
		return "00"+s;
	}
	if(s.length()==3){
		return "0"+s;
	}
	if(s.length()==4){
		return s;
	}
	cout<<"konverzija nije validne duzine parametar je predugacak"<<endl;
	throw 1;
}

string Konvert::vratiHexNa2B(string s){
	s = konvertujUnIntToHex(konvertujStringToUnInt(s));
	if(s.length()==1){
		return "0"+s;
	}
	if(s.length()==2){
		return s;
	}
	cout<<"konverzija nije validne duzine! - Neki parametar je predugacak"<<endl;
	throw 1;
}

string Konvert::binToHex(string c){
	if(c.compare("0000")==0) return "0";
	if(c.compare("0001")==0) return "1";
	if(c.compare("0010")==0) return "2";
	if(c.compare("0011")==0) return "3";
	if(c.compare("0100")==0) return "4";
	if(c.compare("0101")==0) return "5";
	if(c.compare("0110")==0) return "6";
	if(c.compare("0111")==0) return "7";
	if(c.compare("1000")==0) return "8";
	if(c.compare("1001")==0) return "9";
	if(c.compare("1010")==0) return "A";
	if(c.compare("1011")==0) return "B";
	if(c.compare("1100")==0) return "C";
	if(c.compare("1101")==0) return "D";
	if(c.compare("1110")==0) return "E";
	if(c.compare("1111")==0) return "F";
	cout<<"nije moguce pretvoriti ovaj binarni broj u hex!"<<endl;
    throw 1;
}

string Konvert::binaryToHex8(string s){
	string bin=binToHex(s.substr(0,4));
	bin+=binToHex(s.substr(4,4));
    return bin;
}

bool Konvert::proveraHexVrednosti(string s){
	if(s.length()<3)
		return false;
	if(s.substr(0,2).compare("0x")==0){
		for(unsigned int i=2; i<s.length();i++){
			size_t found = hexCifre.find(s[i]);
			if (found==std::string::npos)
				return false; 
		}
		return true;
	}else
		return false;
}

bool Konvert::broj(const std::string& s){
    std::string::const_iterator it = s.begin();
    while (it != s.end() && std::isdigit(*it)) ++it;
    return !s.empty() && it == s.end();
}

string Konvert::konvertujUnIntToHex(unsigned int i){
	stringstream stream;
	stream << hex << i;
	return velikaSlova(stream.str());
}

string Konvert::konvertujIntToHex(int i){
	stringstream stream;
	stream << hex << i;
	return velikaSlova(stream.str());
}

string Konvert::velikaSlova(string s){
	transform(s.begin(), s.end(),s.begin(), ::toupper);
	return s;
}
