#ifndef __Sekcije_H_
#define __Sekcije_H_

#include <iostream>
#include <cstring>
#include <vector>

using namespace std;

class Sekcije {
	private:
		static vector<string> sekcije;
		static vector<string> init();
	public:
		static bool pronadji(string s);
};

#endif 


