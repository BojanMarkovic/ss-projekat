#ifndef __TabelaRelokacije_H_
#define __TabelaRelokacije_H_

#include <iostream>
#include <cstring>
#include <vector>

using namespace std;

class TabelaRelokacije {
	private:
		string sekcija, tip, hexOfset;
		unsigned int brSimbola;
		static vector<TabelaRelokacije> nizTabelaRelokacije;
		TabelaRelokacije(TabelaRelokacije *t);
		TabelaRelokacije(string s, string t, unsigned int b, string h);
	public:
		static void dodaj(string s, string t, unsigned int b, string h);
		static void ispis();
};

#endif 

					
