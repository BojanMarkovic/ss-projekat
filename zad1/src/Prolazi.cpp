#include "Prolazi.h"
#include <cstdlib>
#include <algorithm>
#include <fstream>
#include "Sekcije.h"
#include "Instrukcije.h"
#include "TabelaSimbola.h"
#include "TabelaSekcija.h"
#include "Konvert.h"
#include "TabelaRelokacije.h"

char* Prolazi::ulazFajl= NULL;
char* Prolazi::izlazniFajl= NULL;
vector<jednaLinija> Prolazi::tokeni;
int Prolazi::globalLinija=0;
string Prolazi::trenutnaSekcija="";
ofstream Prolazi::izlaz;

void Prolazi::pass1(char *ul){
	TabelaSekcija::init();
	ulazFajl=ul;
	ucitaj();
	for(unsigned int i=0;i<tokeni.size();i++){
		globalLinija=i;
		for(unsigned int j=0;j<tokeni[i].size();j++){
			if(tokeni[i][j].compare(".section")==0){
				if(tokeni[i].size()!=j+3){
					cout<<"greska u broju parametara za .section"<<endl;
					throw globalLinija;
				}
				if(TabelaSimbola::pronadji(tokeni[i][j])){
					cout<<"simbol vec postoji"<<endl;
					throw globalLinija;
				}
				j++;
				trenutnaSekcija=tokeni[i][j];
				TabelaSekcija(tokeni[i][j], tokeni[i][j+1]);
				TabelaSimbola(tokeni[i][j], tokeni[i][j], "L", 0);
				j++;
				continue;
			}
			if(Sekcije::pronadji(tokeni[i][j].substr(1, tokeni[i][j].length()-1))){
				if(tokeni[i].size()!=j+1){
					cout<<"greska u broju parametara za sekcije"<<endl;
					throw globalLinija;
				}
				tokeni[i][j].erase(0,1);
				trenutnaSekcija=tokeni[i][j];
				if(TabelaSimbola::pronadji(tokeni[i][j])){
					cout<<"simbol vec postoji"<<endl;
					throw globalLinija;
				}
				TabelaSimbola(tokeni[i][j], tokeni[i][j], "L", 0);
				continue;
			}
			if(tokeni[i][j].compare(".end")==0){
				if(tokeni[i].size()!=j+1){
					cout<<"greska u broju parametara za .end"<<endl;
					throw globalLinija;
				}
				i=tokeni.size();
				break;
			}
			if(tokeni[i][j].compare(".global")==0){
				j++;
				for(;j<tokeni[i].size();j++){
					if(TabelaSimbola::pronadji(tokeni[i][j])){
						cout<<"simbol vec postoji"<<endl;
						throw globalLinija;
					}
					TabelaSimbola(tokeni[i][j],"-1", "G", 0);
				}
				continue;
			}
			if(tokeni[i][j].compare(".extern")==0){
				j++;
				for(;j<tokeni[i].size();j++){
					if(TabelaSimbola::pronadji(tokeni[i][j])){
						cout<<"simbol  vec postoji"<<endl;
						throw globalLinija;
					}
					TabelaSimbola(tokeni[i][j],"UNDEFINED", "G", 0);
				}
				continue;
			}
			if(trenutnaSekcija.compare("")!=0){
				if(tokeni[i][j].find(":")!=string::npos){
					if(tokeni[i][j].substr(tokeni[i][j].length()-1,1)!=":"){
						cout<<"parametar lici na labelu, ali to nije"<<endl;
						throw globalLinija;
					}
					tokeni[i][j].erase(tokeni[i][j].length()-1,1);
					if(TabelaSimbola::pronadji(tokeni[i][j])){
						if(!TabelaSimbola::canADD(tokeni[i][j], trenutnaSekcija, 
							TabelaSekcija::vratiBrojac(trenutnaSekcija))){
							cout<<"simbol vec postoji"<<endl;
							throw globalLinija;
						}
						continue;
					}
					TabelaSimbola(tokeni[i][j], trenutnaSekcija, "L",
					 TabelaSekcija::vratiBrojac(trenutnaSekcija));
					continue;
				}
				if(tokeni[i][j].compare(".byte")==0){
					if(tokeni[i].size()!=j+2){
						cout<<"greska u broju parametara za .byte"<<endl;
						throw globalLinija;
					}
					j++;
					TabelaSekcija::dodajLiniju(".byte "+tokeni[i][j], trenutnaSekcija);
					TabelaSekcija::uvecajBrojac(1, trenutnaSekcija);
					continue;
				}
				if(tokeni[i][j].compare(".word")==0){
					if(tokeni[i].size()!=j+2){
						cout<<"greska u broju parametara za .word"<<endl;
						throw globalLinija;
					}
					j++;
					TabelaSekcija::dodajLiniju(".word "+tokeni[i][j], trenutnaSekcija);
					TabelaSekcija::uvecajBrojac(2, trenutnaSekcija);
					continue;
				}
				if(tokeni[i][j].compare(".align")==0){
					if(tokeni[i].size()!=j+2){
						cout<<"greska u broju parametara za .align"<<endl;
						throw globalLinija;
					}
					j++;
					if(Konvert::proveraHexVrednosti(tokeni[i][j])){
						TabelaSekcija::dodajLiniju(".align "+tokeni[i][j], trenutnaSekcija);
						TabelaSekcija::uvecajBrojac(Konvert::konvertujStringToUnInt(tokeni[i][j]), trenutnaSekcija);
						continue;
					}else{
						cout<<"dati parametar nije hex vrednost"<<endl;
						throw globalLinija;
					}
				}
				if(tokeni[i][j].compare(".skip")==0){
					if(tokeni[i].size()!=j+2){
						cout<<"greska u broju parametara za .skip"<<endl;
						throw globalLinija;
					}
					j++;
					if(Konvert::proveraHexVrednosti(tokeni[i][j])){
						TabelaSekcija::dodajLiniju(".skip "+tokeni[i][j], trenutnaSekcija);
						TabelaSekcija::uvecajBrojac(Konvert::konvertujStringToUnInt(tokeni[i][j]), trenutnaSekcija);
						continue;
					}else{
						cout<<"dati parametar nije hex vrednost"<<endl;
						throw globalLinija;
					}
				}
				if(tokeni[i][j].compare(".equ")==0){
					if(tokeni[i].size()!=j+3){
						cout<<"greska u broju parametara za .equ"<<endl;
						throw globalLinija;
					}
					j++;
					if(Sekcije::pronadji(tokeni[i][j])){
						cout<<"nije dozvoljena kombinacija parametara"<<endl;
						throw globalLinija;
					}
					if(TabelaSimbola::pronadji(tokeni[i][j])){
						cout<<"simbol vec postoji"<<endl;
						throw globalLinija;
					}
					if(TabelaSimbola::pronadji(tokeni[i][j+1])){
						TabelaSimbola(tokeni[i][j], TabelaSimbola::getSekcija(tokeni[i][j+1]), 
							TabelaSimbola::getLokal(tokeni[i][j+1]),
							TabelaSimbola::getOfset(tokeni[i][j+1]));
					}else{
						if(!Konvert::proveraHexVrednosti(tokeni[i][j+1])){
							cout<<"nije validan zapis .equ parametara"<<endl;
							throw globalLinija;
						}
						TabelaSimbola(tokeni[i][j], trenutnaSekcija, "L",
							Konvert::konvertujStringToUnInt(tokeni[i][j+1]));
						TabelaSimbola::setABS(tokeni[i][j]);
					}
					j++;
					continue;
				}
				string  inst = tokeni[i][j];
				if (inst.length() > 4) {
					inst.erase(inst.length()-1, 1);
				}else if (inst.length() > 3) {
					if (inst.substr(inst.length()-1, 1).compare("b") == 0){
						inst.erase(inst.length()-1, 1);
					}
					if (inst.substr(inst.length()-1, 1).compare("w") == 0)
						inst.erase(inst.length()-1, 1);
				}
				if(Instrukcije::pronadji(inst)){
					if(tokeni[i].size()>j+3){
						cout<<"greska u broju parametara za instrukcije"<<endl;
						throw globalLinija;
					}
					string x="";
					for(unsigned int q=j;q<tokeni[i].size();q++){
						x+=tokeni[i][q]+" ";
					}
					TabelaSekcija::dodajLiniju(x, trenutnaSekcija);
					try{
						TabelaSekcija::uvecajBrojac(Instrukcije::izracunajBrojac(x), trenutnaSekcija);
					}catch(int x){
						cout<<"greska u instrukciji"<<endl;
						throw globalLinija;
					}
					break;
				}
			}
			cout<<"globalna greska, lose rasporedjene sekcije, bez smisla kod"<<endl;
			throw globalLinija;
		}
	}
}
void Prolazi::pass2(char *iz){
	izlazniFajl=iz;
	izlaz.open(izlazniFajl, ios::out|ios::binary);
	TabelaSekcija::razresiSe();
	TabelaSekcija::izlaz();
	TabelaSimbola::ispis();
	TabelaRelokacije::ispis();
	izlaz.close();
	cout<<"************************************************************"<<endl;
}

void Prolazi::ucitaj(){
	ifstream ulazni(ulazFajl);
	string linija;
	while(getline(ulazni, linija)){
		unsigned int found = linija.find(';');
		if(found!=string::npos){
			linija= linija.substr(0, found);
		}
		vector<string> tokenLinija;
		split(linija, " ,\t\n", tokenLinija);
		
		if(tokenLinija.size()==0)
			continue;
		tokeni.push_back(tokenLinija);
	}
	ulazni.close();
}

void Prolazi::split(const string &s, const char* delimit, vector<string> & v){
    char * dup = strdup(s.c_str());
    char * token = strtok(dup, delimit);
    while(token != NULL){
        v.push_back(string(token));
        token = strtok(NULL, delimit);
    }
    free(dup);
}
