#include "TabelaRelokacije.h"
#include "Prolazi.h"
#include <fstream>

vector<TabelaRelokacije> TabelaRelokacije::nizTabelaRelokacije;

TabelaRelokacije::TabelaRelokacije(string s, string t, unsigned int b, string h) {
    sekcija = s;
    tip = t;
    brSimbola = b;
    hexOfset = h;
    nizTabelaRelokacije.push_back(this);
}

TabelaRelokacije::TabelaRelokacije(TabelaRelokacije *t) {
    sekcija = t->sekcija;
    tip = t->tip;
    brSimbola = t->brSimbola;
    hexOfset = t->hexOfset;
}

void TabelaRelokacije::dodaj(string s, string t, unsigned int b, string h) {
    TabelaRelokacije(s, t, b, h);
}

void TabelaRelokacije::ispis() {
    Prolazi::izlaz << "TabelaRelokacije" << endl;
    for (unsigned int i = 0; i < nizTabelaRelokacije.size(); i++)
        Prolazi::izlaz << nizTabelaRelokacije[i].sekcija << "	" << nizTabelaRelokacije[i].tip << "	" <<
                       nizTabelaRelokacije[i].hexOfset << "	" << nizTabelaRelokacije[i].brSimbola << endl;
}
