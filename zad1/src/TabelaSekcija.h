#ifndef __TabelaSekcija_H_
#define __TabelaSekcija_H_

#include <iostream>
#include <cstring>
#include <vector>

using namespace std;

typedef struct {
    string sadrzajHex;
    unsigned int ofset;
} izlazPRW;

class TabelaSekcija {
	private:
		vector<izlazPRW> nizPodataka;//za PRW sekciju
		vector<string> kodovanInstr;
		vector<string> tokeniSekcije;
		string ime, flegovi;
		unsigned int brojac, velicina;
		static vector<TabelaSekcija> nizTabelaSekcija;
		TabelaSekcija(TabelaSekcija *t);
		static void razresiSePRW(TabelaSekcija &t);
		static void razresiSePXR(TabelaSekcija &t);
		static void doSmth(TabelaSekcija &t, string op1, bool bitPomer8);
		static void pomocnaFunkcijaAdresiranje1(string op, TabelaSekcija &t, string x);
	public:
		TabelaSekcija(string j, string f);
		unsigned int getBrojac();
		static void init();
		void resetBrojac();
		string getIme();
		string getFlegovi();
		static unsigned int vratiBrojac(string t);
		static void dodajLiniju(string s, string t);
		static void uvecajBrojac(unsigned int j, string t);
		static bool proveraBssTip(string t);
		static void razresiSe();
		static void izlaz();
};

#endif 

