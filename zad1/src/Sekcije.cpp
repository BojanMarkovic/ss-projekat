#include "Sekcije.h"
#include <algorithm>

vector<string> Sekcije::sekcije = init();

vector<string> Sekcije::init() {
    vector<string> s;
    s.push_back("text");
    s.push_back("data");
    s.push_back("bss");
    return s;
}

bool Sekcije::pronadji(string s) {
    return find(sekcije.begin(), sekcije.end(), s) != sekcije.end();
}
