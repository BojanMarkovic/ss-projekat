#ifndef __Prolazi_H_ 
#define __Prolazi_H_
#include <cstring>
#include <iostream>
#include <vector>
using namespace std;

typedef vector<string> jednaLinija;

class Prolazi{
	private:
		static char* ulazFajl;
		static char* izlazniFajl;
		static int globalLinija;
		static string trenutnaSekcija;
		static void ucitaj();
		static vector<jednaLinija> tokeni;
	public:
		static void split(const string &s, const char* delimit, vector<string> & v);
		static void pass1(char *ul);
		static void pass2(char *iz);
		static ofstream izlaz;
};

#endif 
